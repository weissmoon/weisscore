package weissmoon.core.block;

import net.minecraft.block.Block;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.helper.WeissBlockRegistry;

/**
 * Base block
 * automatically register: modID, registry name, unlocalized name, into mod BlockRegistry for model registration
 * Created by Weissmoon on 11/13/16.
 */
public class WeissBlock extends Block implements IBlockWeiss{

    private final String ModId;
    protected final String RegName;

    public WeissBlock(String name, Properties properties){
        super(properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
    }

    @Override
    public final String getModID(){
        return ModId;
    }

    @Override
    public final String getWeissName(){
        return RegName;
    }
}
