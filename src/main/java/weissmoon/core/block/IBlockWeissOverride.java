package weissmoon.core.block;

import net.minecraft.item.ItemStack;

/**
 * Created by Weissmoon on 1/11/20.
 */
public interface IBlockWeissOverride extends IBlockWeiss{

    default String getOriginalModID(ItemStack itemStack){
        return getModID();
    }

    default String getCreatorModId(ItemStack itemStack){
        return getTrueModID(itemStack);
    }

    String getTrueModID(ItemStack itemStack);
}
