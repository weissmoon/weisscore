package weissmoon.core.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.PressurePlateBlock;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemGroup;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.helper.WeissBlockRegistry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Weissmoon on 5/24/19.
 */
public abstract class WeissBlockPressurePlate extends PressurePlateBlock implements IBlockWeiss{

    private final String ModId;
    protected final String RegName;

    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;

    public WeissBlockPressurePlate(String name, Sensitivity sensitivity, Properties properties){
        super(sensitivity, properties.doesNotBlockMovement());
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        this.setDefaultState(this.stateContainer.getBaseState().with(POWERED, false));
        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
    }

    @Override
    protected int computeRedstoneStrength(World worldIn, BlockPos pos){
        List<? extends Entity> list;

        list = this.getEntitylist(worldIn, pos);

        if(list.size() <= 0)
            return 0;

        for (Entity entity : list)
            if (!entity.doesEntityNotTriggerPressurePlate())
                return 15;

        return 0;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder){
        builder.add(POWERED);
    }

    @Override
    protected int getRedstoneStrength(BlockState state){
        return state.get(POWERED) ? 15 : 0;
    }

    @Override
    protected BlockState setRedstoneStrength(BlockState state, int strength){
        return state.with(POWERED, strength > 0);
    }

    @Override
    public String getModID(){
        return ModId;
    }

    @Override
    public String getWeissName(){
        return RegName;
    }

    @Override
    public Collection<ItemGroup> getCreativeTabs(){
        List<ItemGroup> list  = new ArrayList<>();
        list.add(ItemGroup.DECORATIONS);
        list.add(ItemGroup.REDSTONE);
        return list;
    }

    /**
     * @param world World which block resides in
     * @param pos Position of block in world
     * @return List of entities which can activate pressure plate
     */
    protected abstract List<? extends Entity> getEntitylist(World world, BlockPos pos);
}
