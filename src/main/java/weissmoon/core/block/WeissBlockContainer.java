package weissmoon.core.block;

import net.minecraft.block.ContainerBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.helper.WeissBlockRegistry;

/**
 * Base block which must contain TileEntity
 * automatically register: modID, registry name, unlocalized name, into mod BlockRegistry for model registration
 * Created by Weissmoon on 11/13/16.
 */
public abstract class WeissBlockContainer extends ContainerBlock implements IBlockWeiss{

    private final String ModId;
    protected final String RegName;

    protected WeissBlockContainer(String name, Properties properties){
        super(properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
    }

    @Override
    abstract public TileEntity createNewTileEntity(IBlockReader var1);

    @Override
    public final String getModID(){
        return ModId;
    }

    @Override
    public final String getWeissName(){
        return RegName;
    }
}
