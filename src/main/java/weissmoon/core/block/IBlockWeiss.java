package weissmoon.core.block;

import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;

import java.util.Collection;
import java.util.Collections;

/**
 * Helper interface for Blocks, used to automatically perform  certain tanks
 * Created by Weissmoon on 11/13/16.
 */
public interface IBlockWeiss{

    /**
     * @return registry mod ID
     */
    String getModID();

    /**
     * @return registry name
     */
    String getWeissName();

    /**
     * @return Does the block have a BlockItem
     */
    default boolean hasCustomBlockItem(){
        return false;
    }

    /**
     * @return BlockItem of the block
     */
    default BlockItem getCustomBlockItem(){
        return null;
    }

    /**
     * Automatically add all blocks to the Miscellaneous Creative tab
     */
    default Collection<ItemGroup> getCreativeTabs(){
        return Collections.singleton(ItemGroup.MISC);
    }

    /*
     * This is now better handled by custom json models
     * Used to register Block model/StateMapper
     * @param iIconRegister instance to call
     */
    //@OnlyIn(Dist.CLIENT)
    //void registerBlockIcons(IIconRegister iIconRegister);
}
