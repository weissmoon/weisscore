package weissmoon.core.client.sound;

import net.minecraft.client.audio.*;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Plays a sound that follows an entity.
 * TODO class broken
 * Similar to minecart sound.
 */
@OnlyIn(Dist.CLIENT)
public class BoundSoundCore extends LocatableSound implements ITickableSound{

    protected Entity entity;
    protected boolean isDone = false;

    public BoundSoundCore(ResourceLocation p_i45103_1_, SoundCategory category, Entity entity, float loudness, float pitch){
        super(p_i45103_1_, category);
        this.entity = entity;
        this.pitch = pitch;
        this.volume = loudness;
    }

    public BoundSoundCore(ResourceLocation p_i45103_1_, SoundCategory category){
        super(p_i45103_1_, category);
    }


    @Override
    public double getX(){
        return this.entity.getPositionVec().x;
    }

    @Override
    public double getY(){
        return this.entity.getPositionVec().y;
    }

    @Override
    public double getZ(){
        return this.entity.getPositionVec().z;
    }


    @Override
    public boolean isDonePlaying(){
        return this.isDone;
    }

    @Override
    public void tick(){
    }
}
