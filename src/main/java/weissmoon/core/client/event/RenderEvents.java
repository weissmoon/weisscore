package weissmoon.core.client.event;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.core.client.render.renderOverride.CustomRenderRegistry;
import weissmoon.core.api.client.item.IItemRenderer;

/**
 * Used to render IItemRender items sooner avoiding vanilla model code
 * Created by Weissmoon on 9/21/16.
 */
public class RenderEvents{

    @SubscribeEvent
    public void renderFirstPerson(RenderHandEvent event){
        Minecraft mc = Minecraft.getInstance();
        ItemStack a = mc.player.getHeldItem(event.getHand());

        if(a == ItemStack.EMPTY)
            return;
        HandSide side;
        if(mc.player.getPrimaryHand() == HandSide.RIGHT){
            if(event.getHand() == Hand.MAIN_HAND){
                side = HandSide.RIGHT;
            }else{
                side = HandSide.LEFT;
            }
        }else{
            if (event.getHand() == Hand.MAIN_HAND){
                side = HandSide.LEFT;
            }else{
                side = HandSide.RIGHT;
            }
        }

        ItemCameraTransforms.TransformType renderSide = side == HandSide.RIGHT?
                ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND:
                ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND;
        IItemRenderer itemRenderer = CustomRenderRegistry.getItemRenderer(mc.player.getHeldItem(event.getHand()), renderSide);
        if(itemRenderer == CustomRenderRegistry.getMissingRender())
            return;

        event.setCanceled(true);
        MatrixStack matrixStack = event.getMatrixStack();
        matrixStack.push();
        try{
            matrixStack.translate(0.0F, -0.8F, 0.0F);
            matrixStack.rotate(new Quaternion(0, 25.0F, 0.0F, true));
            if(itemRenderer.shouldUseRenderHelper(renderSide, mc.player.getHeldItem(event.getHand()), IItemRenderer.ItemRendererHelper.EQUIPT_ANIMATION))
                this.transformSideFirstPerson(side, event.getEquipProgress(), matrixStack);
            else
                this.transformSideFirstPerson(side, 0.0F, matrixStack);
            this.transformFirstPerson(side, event.getSwingProgress(), matrixStack);
            this.renderItem(itemRenderer,renderSide, mc.player.getHeldItem(event.getHand()), event.getMatrixStack(), event.getBuffers(), event.getLight());
        }finally{
            matrixStack.pop();
        }
    }

    public void renderItem(IItemRenderer itemRenderer, ItemCameraTransforms.TransformType type, ItemStack item, MatrixStack matrixStack, Object... data){
        itemRenderer.renderItem(type, item, matrixStack, data);
    }

    private void transformSideFirstPerson(HandSide p_187459_1_, float p_187459_2_, MatrixStack matrixStack){
        int i = p_187459_1_ == HandSide.RIGHT ? 1 : -1;
        matrixStack.translate((float)i * 0.56F, -0.52F + p_187459_2_ * -0.6F, -0.72F);
    }

    private void transformFirstPerson(HandSide p_187453_1_, float p_187453_2_, MatrixStack matrixStack){
        int i = p_187453_1_ == HandSide.RIGHT ? 1 : -1;
        if(p_187453_1_ == HandSide.LEFT){
            matrixStack.translate(-0.5890319049358367919921874F, -0.47985F, -1.8007F);
            matrixStack.rotate(new Quaternion(0, 20F, 0F, true));
        }else{
            matrixStack.translate(0.8279F, -0.47985F, -0.6183F);
            matrixStack.rotate(new Quaternion(0, 20F, 0F, true));
        }
        float f = MathHelper.sin(p_187453_2_ * p_187453_2_ * (float)Math.PI);
        matrixStack.rotate(new Quaternion(0, (float)i * (45.0F + f * -20.0F), 0.0F, true));
        float f1 = MathHelper.sin(MathHelper.sqrt(p_187453_2_) * (float)Math.PI);
        matrixStack.rotate(new Quaternion(0, 0, (float)i * f1 * -20.0F, true));
        matrixStack.rotate(new Quaternion(f1 * -80.0F, 1.0F, 0.0F, true));
        matrixStack.rotate(new Quaternion(0, (float)i * -45.0F, 0.0F, true));
    }

}
