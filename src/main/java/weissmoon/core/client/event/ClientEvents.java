package weissmoon.core.client.event;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraft.item.*;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.api.client.item.IHUDItem;
import weissmoon.core.handler.ConfigurationHandler;
import weissmoon.core.api.client.helper.HUDHelper;
import weissmoon.core.helper.MaterialStringHelper;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.glfwGetKey;

/**
 * Class for client side events
 */
public class ClientEvents{

    public Minecraft minecraft = Minecraft.getInstance();

    /**
     * Displays ItemBlock material and translates using .lang files
     * Also display food for wolves.
     */
    @SubscribeEvent
    public void tooltipEvent(ItemTooltipEvent event){
        //Shift keys
        //KeyboardListener
        boolean shiftPressed = ((glfwGetKey(minecraft.mainWindow.getHandle(), 340) == GLFW_PRESS) || (glfwGetKey(minecraft.mainWindow.getHandle(),344)== GLFW_PRESS));
        if(shiftPressed){
            Item item = event.getItemStack().getItem();
            //Displays ItemBlock material and translates using .lang files
            if(ConfigurationHandler.Client.materialEvent.get())
                if(item instanceof BlockItem){
                    TranslationTextComponent ds = new TranslationTextComponent("weisscore.material.material");
                    event.getToolTip().add(ds);
                    event.getToolTip().add(MaterialStringHelper.getMaterialString(((BlockItem)item).getBlock().getDefaultState().getMaterial()));
                }
            //display food for wolves.
            if(ConfigurationHandler.Client.wolfTip.get())
                if(item.isFood())
                    if((item.getFood()).isMeat()){
                        TranslationTextComponent ds = new TranslationTextComponent("weisscore.wolfFood");
                        event.getToolTip().add(ds);
                    }
        }
        //Hide curse of binding
        if(ConfigurationHandler.Client.stealthBinding.get()){
            //Show curse of binding if player is in creative mode
            if(Minecraft.getInstance().player != null && Minecraft.getInstance().player.abilities.isCreativeMode)
                return;

            ItemStack stack = event.getItemStack();

            if(stack.hasTag()){
                //Show curse of binding for tagged items
                //Items are tagged when equipped
                if(stack.getTag().contains("WMShowCurse"))
                    return;

                int i1 = 0;
                //Skip hiding if Enchantments are hidden
                if(stack.getTag().contains("HideFlags", 99))
                    i1 = stack.getTag().getInt("HideFlags");

                if((i1 & 1) == 0){
                    ListNBT nbttaglist = stack.getEnchantmentTagList();

                    for(int j=0;j < nbttaglist.size();++j){
                        CompoundNBT nbttagcompound = nbttaglist.getCompound(j);
                        String k = nbttagcompound.getString("id");
                        //Only remove Curse of Binding tooltip
                        //"ID:10 = Curse of binding"
                        if(!k.equals("minecraft:binding_curse"))
                            continue;

                        int l = nbttagcompound.getShort("lvl");
                        Enchantment enchantment = Enchantment.getEnchantmentByID(10);

                        if(enchantment != null)
                            event.getToolTip().remove(enchantment.getDisplayName(l));
                    }
                }
            }
        }
    }

    private static int tickTimeHUD = 0;
    private static boolean hadHUD = false;
    private static IHUDItem previousHUD = null;
    private static ItemStack preciousStack = ItemStack.EMPTY;
    private static IHUDItem currentHUD = null;
    private static ItemStack currentStack = ItemStack.EMPTY;
    private static float partialTick = 0;

    /**
     * Render sdditional screen elements of {@link IHUDItem}
     */
    @SubscribeEvent
    public void renderScreen(RenderGameOverlayEvent.Post event){
        if(event.getType() == RenderGameOverlayEvent.ElementType.ALL){
            minecraft.getProfiler().startSection("RWBY-hud");
            MatrixStack matrixStack = event.getMatrixStack();
            matrixStack.push();
            PlayerEntity player = minecraft.player;
            if(player == null)
                return;

            partialTick = event.getPartialTicks();
            if(previousHUD != null){
                hadHUD = true;
                if(previousHUD.doRenderHUD(preciousStack)){
                    if(previousHUD.isSimple(preciousStack)){
                        ItemStack stack = previousHUD.getDisplayStack(preciousStack);
                        HUDHelper.renderItemOppositeOffhandHud(matrixStack, stack, String.valueOf(stack.getCount()), 16777215);
                    }else
                        previousHUD.renderPreviousHUD(matrixStack, preciousStack);
                }else{
                    tickTimeHUD = 1;
                }
            }else if(currentHUD != null){
                hadHUD = false;
                if(currentHUD.doRenderHUD(currentStack)){
                    if(currentHUD.isSimple(currentStack)){
                        ItemStack stack = currentHUD.getDisplayStack(currentStack);
                        HUDHelper.renderItemOppositeOffhandHud(matrixStack, stack, String.valueOf(stack.getCount()), 16777215);
                    }else
                        currentHUD.renderHUD(matrixStack, currentStack);
                }
            }

//            ItemStack heldStack = player.getItemStackFromSlot(EquipmentSlotType.MAINHAND);
//            if(heldStack.getItem() instanceof IHUDItem || WeissCoreAPI.hudItemRegistry.getIHUDItem(heldStack.getItem()) != null){
//                IHUDItem ihudItem =  WeissCoreAPI.hudItemRegistry.getIHUDItem(heldStack.getItem());
//                if(ihudItem.doRenderHUD(heldStack)){
//                    MatrixStack matrixStack = event.getMatrixStack();
//                    if(ihudItem.isSimple(heldStack)){
//                        ItemStack stack = ihudItem.getDisplayStack(heldStack);
//                        HUDHelper.renderItemOppositeOffhandHud(matrixStack, stack, String.valueOf(stack.getCount()), 16777215);
//                    }else
//                        ihudItem.renderHUD(matrixStack, heldStack);
//                }
//            }
            matrixStack.pop();
            minecraft.getProfiler().endSection();
        }
    }

    @SubscribeEvent
    public void setTickTimeHUD(TickEvent.PlayerTickEvent event){
        minecraft.getProfiler().startSection("RWBY-hud");
        PlayerEntity player = minecraft.player;
        if(player == null)
            return;

        if(previousHUD != null){
            if(tickTimeHUD > previousHUD.getMaxTickOut(preciousStack))
                tickTimeHUD = Math.max(1, previousHUD.getMaxTickOut(preciousStack));
            tickTimeHUD--;
            if(tickTimeHUD > 0){
                ItemStack heldStack = player.getItemStackFromSlot(EquipmentSlotType.MAINHAND);
                if(heldStack.getItem() instanceof IHUDItem || WeissCoreAPI.hudItemRegistry.getIHUDItem(heldStack.getItem()) != null){
                    IHUDItem ihudItem = WeissCoreAPI.hudItemRegistry.getIHUDItem(heldStack.getItem());
                    if(previousHUD == ihudItem){
                        previousHUD = null;
                        preciousStack = ItemStack.EMPTY;
                        currentHUD = ihudItem;
                        currentStack = heldStack;
                        if(tickTimeHUD>currentHUD.getMaxTickIn(currentStack))
                            tickTimeHUD = currentHUD.getMaxTickIn(currentStack);
                    }
                }
                return;
            }else{
                previousHUD = null;
                preciousStack = ItemStack.EMPTY;
            }
        }

        ItemStack heldStack = player.getItemStackFromSlot(EquipmentSlotType.MAINHAND);
        if(heldStack.getItem() instanceof IHUDItem || WeissCoreAPI.hudItemRegistry.getIHUDItem(heldStack.getItem()) != null){
            IHUDItem ihudItem =  WeissCoreAPI.hudItemRegistry.getIHUDItem(heldStack.getItem());
            if(currentHUD == null || currentHUD != ihudItem){
                if(currentHUD != null){
                    tickTimeHUD = currentHUD.getMaxTickOut(currentStack);
                    previousHUD = currentHUD;
                    preciousStack = currentStack;
                }
                if(currentHUD != ihudItem){
                    currentHUD = ihudItem;
                    currentStack = heldStack;
                }
            }else if(ihudItem != null){
                currentStack = heldStack;
            }
        }else if(currentHUD != null){
            tickTimeHUD = currentHUD.getMaxTickOut(currentStack);
            previousHUD = currentHUD;
            currentHUD = null;
            preciousStack = currentStack;
            currentStack = ItemStack.EMPTY;
        }

        if(currentHUD != null && tickTimeHUD < currentHUD.getMaxTickIn(heldStack))
            tickTimeHUD++;
    }

    public static int getTickTImeUp(){
        return tickTimeHUD;
    }

    public static float getPartialTick(){
        return partialTick;
    }

    public static boolean hadHUD(){
        return hadHUD;
    }


    //@SubscribeEvent
    public void recolorClouds (TickEvent.WorldTickEvent event){
    }
}
