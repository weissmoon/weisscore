package weissmoon.core.client.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
//import net.minecraftforge.client.model.IModel;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.client.model.obj.OBJModel;
import weissmoon.core.lib.TexturesCore;

import java.io.FileNotFoundException;

/**
 * Created by Weissmoon on 2/16/18.
 */
public class OBJUtils {

    public static OBJUtils INSTANCE = new OBJUtils();

    //Copied from OBJLoader
//    public IModel loadModel(ResourceLocation modelLocation) throws Exception
//    {
//        ResourceLocation file = modelLocation;
//        IResourceManager manager = Minecraft.getInstance().getResourceManager();
//        IResource resource;
//        try
//        {
//            resource = manager.getResource(file);
//        }
//        catch (FileNotFoundException e)
//        {
//            if (modelLocation.getNamespace().startsWith("models/block/"))
//                resource = manager.getResource(new ResourceLocation(file.getNamespace(), "models/item/" + file.getPath().substring("models/block/".length())));
//            else if (modelLocation.getNamespace().startsWith("models/item/"))
//                resource = manager.getResource(new ResourceLocation(file.getNamespace(), "models/block/" + file.getPath().substring("models/item/".length())));
//            else throw e;
//        }
//        OBJModel.Parser parser = new OBJModel.Parser(resource, manager);
//        OBJModel model = null;
//        Exception sd = new Exception();
//        try
//        {
//            model = parser.parse();
//        }
//        catch (Exception e)
//        {
//            sd = e;
//        }
//        finally
//        {
//        }
//        if (model == null) throw new ModelLoaderRegistry.LoaderException("Error loading model previously: " + file, sd);
//        return model;
//    }
}
