package weissmoon.core.client.util;

import java.awt.*;

public class ColorHelper{

    /**
     * Used o interpolate the colour
     * @param origin rigial colour to interpolate
     * @param destination target colour
     * @param factor percentage between origin colour and destination colour
     */
    public static Color interpolatedColor(Color origin, Color destination, float factor){
        float x = (1 - factor) * origin.getRed() + factor * destination.getRed();
        float y = (1 - factor) * origin.getGreen() + factor * destination.getGreen();
        float z = (1 - factor) * origin.getBlue() + factor * destination.getBlue();
        float w = (1 - factor) * origin.getAlpha() + factor * destination.getAlpha();
        return new Color(x, y, z, w);
    }

    /**
     * Make a copy of a Color instance that can be freely manipulated
     * @param color input color instance to copy
     * @return new copy of  color instance
     */
    public static Color copyColor(Color color){
        return new Color(color.getColorSpace(), color.getComponents(null), color.getTransparency());
    }
}
