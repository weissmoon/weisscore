package weissmoon.core.client.util;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.Tessellator;

import java.awt.*;

public class ItemRender{

    /**
     * used to render a damage bar on the screen
     * @param x left pixel of item
     * @param y top pixel of item
     * @param maxDam maximum damage a n item can cave
     * @param dispDamage displayed damage an item is showing
     * @param full int colour for full damage
     * @param empty int colour for empty damage
     * @param back int colour for background
     *             input colours are 0xaarrggbb format
     */
    public static void renderBar(int x, int y, double maxDam, double dispDamage, Color full, Color empty, Color back){
        double ratio = dispDamage / maxDam;
        Color foregroundColour = ColorHelper.copyColor(full);
        Color emptyColour = ColorHelper.copyColor(empty);
        foregroundColour = ColorHelper.interpolatedColor(foregroundColour, emptyColour, (float)ratio);
        Color backgroudColour = ColorHelper.copyColor(back);
        ColorHelper.interpolatedColor(backgroudColour, foregroundColour, 0.15f);

        int barLength = (int)Math.round(12.0 * (1 - ratio));

        renderQuad2D(x + 2, y + 12, 0, 12, 1, backgroudColour);
        renderQuad2D(x + 2, y + 12, 0, barLength, 1, foregroundColour);
    }

    public static void renderQuad2D(double x, double y, double z, double width, double height, Color colorRGBA){
        float red, green, blue, alpha;
        red = colorRGBA.getRed();
        green = colorRGBA.getGreen();
        blue = colorRGBA.getBlue();
        alpha = colorRGBA.getAlpha();
//        GL11.glDisable(GL11.GL_TEXTURE_2D);
        RenderSystem.disableTexture();
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder renderer = tessellator.getBuffer();
        renderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        renderer.pos(x, y + height, z).color(red, green, blue, alpha).endVertex();
        renderer.pos(x + width, y + height, z).color(red, green, blue, alpha).endVertex();
        renderer.pos(x + width, y, z).color(red, green, blue, alpha).endVertex();
        renderer.pos(x, y, z).color(red, green, blue, alpha).endVertex();
        tessellator.draw();
//        GL11.glEnable(GL11.GL_TEXTURE_2D);
        RenderSystem.enableTexture();
    }
}
