package weissmoon.core.client.creativetab;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

/**
 * Base creative tab
 */
public class CreativeTabWeiss extends ItemGroup {
    private String tabLabel;
    private ItemStack tabItem;

    public CreativeTabWeiss (String label, ItemStack item){
        super(label);
        this.tabLabel = label;
        this.tabItem = item;
    }

    public ItemStack createIcon (){
        if (this.tabItem != null){
            return this.tabItem;
        }
        return new ItemStack(Item.getItemById(1));
    }

    public String getTranslatedTabLabel (){
        if (this.tabLabel == null){
            return "entity.Item.name";
        }
        return this.tabLabel;
    }
}