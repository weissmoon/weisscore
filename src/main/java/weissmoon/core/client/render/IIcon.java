package weissmoon.core.client.render;

import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Intermediary class for lazy coding
 * Created by Weissmoon on 12/18/16.
 */
@OnlyIn(Dist.CLIENT)
public class IIcon extends ModelResourceLocation {
    protected IIcon(String... resourceName){
        super(resourceName);
    }
//    protected IIcon(int p_i46078_1_, String... resourceName) {
//        super(resourceName);
//    }

    public IIcon(String p_i46079_1_) {
        super(p_i46079_1_);
    }

    public IIcon(ResourceLocation p_i46080_1_, String p_i46080_2_) {
        super(p_i46080_1_, p_i46080_2_);
    }

    public IIcon(String p_i46081_1_, String p_i46081_2_) {
        super(p_i46081_1_, p_i46081_2_);
    }
}
