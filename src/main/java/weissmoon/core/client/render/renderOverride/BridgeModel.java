package weissmoon.core.client.render.renderOverride;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Quaternion;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.api.client.item.IItemTieredRender;
import weissmoon.core.item.IBlockItemWeiss;
import weissmoon.core.item.IItemWeiss;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.*;
import static net.minecraft.client.settings.GraphicsFanciness.FABULOUS;
import static net.minecraft.client.settings.GraphicsFanciness.FANCY;

/**
 * Dummy model used to render IItemRenderers and pass real item model back to vanilla Renderer
 * Created by Weissmoon on 8/19/16.
 */
public class BridgeModel implements IBakedModel{//}, IPerspectiveAwareModel {

    private DummyOverrides dummyOverides;

    public BridgeModel(){
        this.dummyOverides = new DummyOverrides();
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand){
        return Collections.emptyList();
    }

    @Override
    public boolean isAmbientOcclusion(){
        return false;
    }

    @Override
    public boolean isSideLit(){
        return dummyOverides.stack.getItem() instanceof BlockItem;
    }

    @Override
    public boolean isGui3d(){
        return false;
    }

    @Override
    public boolean isBuiltInRenderer(){
        return false;
    }

    @Override
    public TextureAtlasSprite getParticleTexture(){
        ItemStack stack = this.dummyOverides.stack;
        IItemWeiss item = (IItemWeiss) stack.getItem();
        return Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getModel(
                item.getIcon(stack)).getParticleTexture();
    }

    @Override
    @SuppressWarnings("deprecation")
    public ItemCameraTransforms getItemCameraTransforms(){
        ItemStack stack = this.dummyOverides.stack;
        IItemWeiss item = (IItemWeiss) stack.getItem();
        return Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getModel(
                item.getIcon(stack)).getItemCameraTransforms();
    }

    @Override
    public ItemOverrideList getOverrides(){
        return this.dummyOverides;
    }

    /**
     * IItemRender rendered here
     * @param cameraTransformType where the model is being rendered
     * @return empty model if IItemRenderer is used, real model with perspective if not used
     */
    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType cameraTransformType, MatrixStack matrixStack){
        ItemStack stack = this.dummyOverides.stack;
        IItemRenderer renderer = CustomRenderRegistry.getItemRenderer(stack, cameraTransformType);
        //Should IItemRender be used for this perspective
        if(renderer.handleRenderType(stack, cameraTransformType)){
            matrixStack.push();
            try{
                if(cameraTransformType == THIRD_PERSON_LEFT_HAND || cameraTransformType == THIRD_PERSON_RIGHT_HAND){
                    float f1 = 0;
                    float f13 = 0.8F;
                    matrixStack.translate(0.0F, 0.310F, -0.27F);
                    matrixStack.rotate(new Quaternion(70.0F, 0.0F, 0.0F, true));
                    matrixStack.rotate(new Quaternion(0.0F, -45.0F,  0.0F, true));
                    matrixStack.scale(0.38F,0.38F,0.38F);
                }

                //Fancy rendered is called here if available
                if(renderer instanceof IItemTieredRender){
                    if(Minecraft.getInstance().gameSettings.graphicFanciness == FABULOUS){
                        ((IItemTieredRender) renderer).renderVeryFancy(cameraTransformType, stack, matrixStack);
                    }else if(Minecraft.getInstance().gameSettings.graphicFanciness == FANCY){
                        ((IItemTieredRender) renderer).renderFancy(cameraTransformType, stack, matrixStack);
                    }
                }else{
                //Basic renderer is called here if fancy renderer is not available
                    IRenderTypeBuffer.Impl irendertypebuffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
                    renderer.renderItem(cameraTransformType, stack, matrixStack, irendertypebuffer, 16215641);
                }
            }finally{
                matrixStack.pop();
            }
            return this.dummyOverides.getModel();
        }else if(stack.getItem() instanceof IItemWeiss){
            //return ItemModel with perspective transforms if IItemRender is not used
            IItemWeiss item = (IItemWeiss) stack.getItem();
            if((Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getModel(item.getIcon(stack))) !=
                    (Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getMissingModel())){
                IBakedModel returnPair = net.minecraftforge.client.ForgeHooksClient.handlePerspective(Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getModel(item.getIcon(stack)), cameraTransformType, matrixStack);
                returnPair = returnPair.getOverrides().getOverrideModel(returnPair, stack, dummyOverides.world, dummyOverides.livingBase);
                if(!(item instanceof IBlockItemWeiss))
                    GlStateManager.enableLighting();
                return returnPair;
                //return Pair.of(Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getModelManager().getModel(item.getIcon(stack, MinecraftForgeClient.getRenderPass()))
                        //.getOverrides().handleItemState(Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getModelManager().getModel(item.getIcon(stack, MinecraftForgeClient.getRenderPass())), stack, this.dummyOverides.getWorld(), this.dummyOverides.livingBase), null);
            }
        }
        //return missing model if item is not IItemRender or is IItemWeiss
        return Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getMissingModel();
    }

    //class used to identify item being rendered
    static class DummyOverrides extends ItemOverrideList{

        private ItemStack stack;
        private IBakedModel model;
        private ClientWorld world;
        private LivingEntity livingBase;
        public DummyOverrides(){
            super();
        }

        @Override
        public IBakedModel getOverrideModel(IBakedModel originalModel, ItemStack stack, ClientWorld world, LivingEntity entity){
            this.stack = stack;
            this.model = originalModel;
            this.world = world;
            this.livingBase = entity;
            return originalModel;
        }
        public ItemStack getStack(){
            return stack;
        }
        public IBakedModel getModel(){
            return this.model;
        }
        public ClientWorld getWorld(){
            return this.world;
        }
        public LivingEntity getEntity(){
            return this.livingBase;
        }
    }
}
