package weissmoon.core.client.render.renderOverride;

import com.google.common.collect.Maps;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.lib.Strings;

import java.util.IdentityHashMap;

/**
 * Internal render registry for use by mod
 * Created by Weissmoon on 9/21/16.
 */
@OnlyIn(Dist.CLIENT)
public class CustomRenderRegistry{

    private static IdentityHashMap<Item, IItemRenderer> customItemRenderers = Maps.newIdentityHashMap();
    private static ModelResourceLocation mesh = new ModelResourceLocation(Strings.RESOURCE_PREFIX + "renderoverride", "inventory");
    //public static final IItemRenderer noRenderer = WeissCoreAPI.noRenderer;

    public static void registerItemRenderer(Item item, IItemRenderer renderer){
        if(item != null)
            if(customItemRenderers.get(item) == null)
                customItemRenderers.put(item, renderer);
            else{
                System.out.println("Item " + item + " has already been registered");
                System.out.println("Will not register IItemRender " + renderer + " to item " + item);
            }
    }

    public static IItemRenderer getIItemRenderer(Item item){
        return customItemRenderers.getOrDefault(item, WeissCoreAPI.noRenderer);
    }

    public static IItemRenderer getItemRenderer(ItemStack item, ItemCameraTransforms.TransformType cameraTransformType){
        Item item1 = item.getItem();
        IItemRenderer renderer = customItemRenderers.get(item1);
        if(renderer != null && renderer.handleRenderType(item, cameraTransformType))
            return renderer;
        else{
            renderer = ((PublicRenderRegistry)WeissCoreAPI.renderRegistry).getPublicCustomRenderers().get(item1);

            if (renderer != null && renderer.handleRenderType(item, cameraTransformType))
                return renderer;
        }
        return WeissCoreAPI.noRenderer;
    }

    public static void registerToModelManager(){
        for(Item e: WeissItemRegistry.weissItemRegistry.getItemList())
            Minecraft.getInstance().getItemRenderer().getItemModelMesher().register(e, mesh);

        PublicRenderRegistry publicRenderRegistry = ((PublicRenderRegistry)WeissCoreAPI.renderRegistry);
        for(Item i: publicRenderRegistry.getPublicCustomRenderers().keySet())
            Minecraft.getInstance().getItemRenderer().getItemModelMesher().register(i, mesh);
    }

    public static IItemRenderer getMissingRender(){
        return WeissCoreAPI.noRenderer;
    }
}
