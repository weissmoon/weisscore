package weissmoon.core.client.render.renderOverride;

import com.google.common.collect.Maps;
import net.minecraft.item.Item;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.api.client.render.IRenderRegistry;

import java.util.IdentityHashMap;

/**
 * real external render registry for use by mod
 * Created by Weissmoon on 1/24/18.
 */
public class PublicRenderRegistry implements IRenderRegistry {


    private final IdentityHashMap<Item, IItemRenderer> customItemRenderers = Maps.newIdentityHashMap();

    @Override
    public void registerItemRenderer(Item item, IItemRenderer renderer){
        if(customItemRenderers.get(item) == null)
            customItemRenderers.put(item, renderer);
        else{
            System.out.println("Item " + item + " has already been registered");
            System.out.println("Will not register IItemRender " + renderer + " to item " + item);
        }
    }

    @Override
    public IItemRenderer getIItemRenderer(Item item) {
        IItemRenderer lorenderer = CustomRenderRegistry.getIItemRenderer(item);
        return customItemRenderers.getOrDefault(item, lorenderer);
    }

    IdentityHashMap<Item, IItemRenderer> getPublicCustomRenderers(){
        return this.customItemRenderers;
    }
}
