package weissmoon.core.client.render.renderOverride;

import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.item.ItemStack;
import weissmoon.core.client.render.WeissMesh;
import weissmoon.core.lib.Strings;

/**
 * Dummy mesh Override for items using IItemRenderer
 *
 * Created by Weissmoon on 9/27/16.
 */
@Deprecated
public class WeissOverrideMesh implements WeissMesh {

    public static ModelResourceLocation getModelLocation(ItemStack stack) {
        return new ModelResourceLocation(Strings.RESOURCE_PREFIX + "renderoverride", "inventory");
    }
}
