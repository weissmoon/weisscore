package weissmoon.core.client.render;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import weissmoon.core.helper.RNGHelper;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Public RenderItem methods
 * TODO note differences
 * Created by Weissmoon on 10/10/16.
 */
public class WeissRenderItem {

    private static final ItemColors itemColors = Minecraft.getInstance().getItemColors();

    public void renderModel(IBakedModel model, int color, @Nullable ItemStack stack)
    {
        GlStateManager.pushMatrix();

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexbuffer = tessellator.getBuffer();
        vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR_NORMAL);

        GlStateManager.color4f((color >> 16&0xFF) / 255.0F,
                (color >> 8&0xFF) / 255.0F,
                (color&0xFF) / 255.0F,
                1);
        //color = -1;
        for (Direction enumfacing : Direction.values())
        {
            this.renderQuads(vertexbuffer, model.getQuads(null, enumfacing, RNGHelper.random), color, stack);
        }

        this.renderQuads(vertexbuffer, model.getQuads(null, null, RNGHelper.random), color, stack);
        tessellator.draw();
        if (stack != null && stack.hasEffect())
        {
            //this.renderEffect(model);
        }
        GlStateManager.popMatrix();
    }

    private void renderQuads(BufferBuilder renderer, List<BakedQuad> quads, int color, @Nullable ItemStack stack)
    {
        boolean flag = color == -1 && stack != null;
        int i = 0;

        for (int j = quads.size(); i < j; ++i)
        {
            BakedQuad bakedquad = (BakedQuad)quads.get(i);
            int k = color;

            if (flag && bakedquad.hasTintIndex())
            {
                k = itemColors.getColor(stack, bakedquad.getTintIndex());

//                if (EntityRenderer.anaglyphEnable)
//                {
//                    k = TextureUtil.anaglyphColor(k);
//                }

                k = k | -16777216;
            }

//            net.minecraftforge.client.model.pipeline.LightUtil.renderQuadColor(renderer, bakedquad, k);
//            net.minecraftforge.client.model.pipeline.LightUtil.renderQuadColorSlow(renderer, bakedquad, k);
        }
    }

}
