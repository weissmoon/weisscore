package weissmoon.core.client.render;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.model.ModelLoader;
import weissmoon.core.block.IBlockWeiss;
import weissmoon.core.client.render.renderOverride.CustomRenderRegistry;
import weissmoon.core.helper.WeissBlockRegistry;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.item.IItemWeiss;

import java.util.List;

/**
 * Registry class for automatic model registry
 * Created by Weissmoon on 7/28/16.
 */
@OnlyIn(Dist.CLIENT)
public class IIconRegister{

    public static final IIconRegister INSTANCE = new IIconRegister();

    private boolean item = false;
    //private ItemModelMesher modelMesher = Minecraft.getMinecraft().getRenderItem().getItemModelMesher();

    public void exnste(){
        this.registerWeissItemIcons();
    }

    private void registerWeissItemIcons(){
        List<Item> list = WeissItemRegistry.weissItemRegistry.getItemList();
        IItemWeiss item ;
        for (Item aList : list) {
            item = (IItemWeiss) aList;
            if (item instanceof IItemRenderCustom)
                CustomRenderRegistry.registerItemRenderer((Item) item, ((IItemRenderCustom) item).getIItemRender());
            item.registerIcons(INSTANCE);
        }
        this.item = true;
    }

    public IIcon registerIcon(Item item, String resouceLocation){
        if(!(item instanceof IItemWeiss))return null;
        if(resouceLocation.contains(":"))
            resouceLocation = resouceLocation.substring(resouceLocation.indexOf(":") + 1);
        IIcon icon = new IIcon(((IItemWeiss)item).getModID().toLowerCase() + ":" + resouceLocation, "inventory");
        ModelLoader.addSpecialModel(icon);
        return icon;
    }

    public final boolean initialized(){
        return item;
    }
}
