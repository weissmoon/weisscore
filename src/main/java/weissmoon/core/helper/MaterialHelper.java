package weissmoon.core.helper;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import weissmoon.core.lib.BlockMaterial;

/**
 * Class to identify Block material
 * Created by Weissmoon on 10/29/18.
 */
public class MaterialHelper{

    /**
     * @param itemStack to identify block material, must contain instance of BlockItem or Block
     * @return material of itemstack Block
     */
    public static Material getMaterialFromItemStack(ItemStack itemStack){
        Item item = itemStack.getItem();
        Block itemBlock;
        try{
            itemBlock = ((BlockItem)item).getBlock();
            return getMaterialFromIBlockState(itemBlock.getDefaultState());
        }catch(ClassCastException|Error ignored){
            return BlockMaterial.UNKNOWN;
        }
    }

    /**
     * Get material of block in world
     * @param world to check block
     * @param x coordinate of block
     * @param y coordinate of block
     * @param z coordinate of block
     * @return material of block
     */
    public static Material getMaterialFromCord(World world, int x, int y, int z){
        return getMaterialFromIBlockState(world.getBlockState(new BlockPos(x, y, z)));
    }

    /**
     * @param world to check block
     * @param blockPos containing coordinate of block
     * @return material of block
     */
    public static Material getMaterialFromBlockPos(World world, BlockPos blockPos){
        return getMaterialFromIBlockState(world.getBlockState(blockPos));
    }

    /**
     * @param iBlockState to check material
     * @return material of IBlockState
     */
    public static Material getMaterialFromIBlockState(BlockState iBlockState){
        return iBlockState.getMaterial();
    }
}
