package weissmoon.core.helper;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Created by Weissmoon on 10/29/18.
 */
public class BlockHelper {

    private boolean canSustainAnyPlant(BlockState iBlockState){
        if(iBlockState.getBlock() == Blocks.GRASS ||
                iBlockState.getBlock() == Blocks.SAND ||
                iBlockState.getBlock() == Blocks.CLAY ||
                iBlockState.getBlock() == Blocks.TERRACOTTA ||
                iBlockState.getBlock() == Blocks.BLACK_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.BLUE_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.BROWN_TERRACOTTA||
                iBlockState.getBlock() == Blocks.CYAN_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.GRAY_TERRACOTTA||
                iBlockState.getBlock() == Blocks.GREEN_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.LIGHT_BLUE_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.LIGHT_GRAY_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.LIME_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.MAGENTA_TERRACOTTA||
                iBlockState.getBlock() == Blocks.ORANGE_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.PINK_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.PURPLE_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.RED_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.WHITE_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.YELLOW_TERRACOTTA ||
                iBlockState.getBlock() == Blocks.SOUL_SAND ||
                iBlockState.getBlock() == Blocks.FARMLAND ||
                iBlockState.getBlock() == Blocks.DIRT)
            return true;
        return false;
    }

    public static void dropStackInWorld(World worldIn, BlockPos pos, ItemStack itemStack){
        if (!itemStack.isEmpty()) {
            float f = 0.7F;
            double d0 = (double)(worldIn.rand.nextFloat() * 0.7F) + (double)0.15F;
            double d1 = (double)(worldIn.rand.nextFloat() * 0.7F) + (double)0.060000002F + 0.6D;
            double d2 = (double)(worldIn.rand.nextFloat() * 0.7F) + (double)0.15F;
            ItemStack itemstack1 = itemStack.copy();
            ItemEntity itementity = new ItemEntity(worldIn, (double)pos.getX() + d0, (double)pos.getY() + d1, (double)pos.getZ() + d2, itemstack1);
            itementity.setDefaultPickupDelay();
            worldIn.addEntity(itementity);
        }
    }
}
