package weissmoon.core.helper;

import java.util.Random;

/**
 * Various random generator results
 */
public class RNGHelper{

    public static final Random random = new Random();

    /**
     * @return random integer value
     */
    public static int getInt(){
        return random.nextInt();
    }

    /**
     * @param top highest integer value to return (must be positive)
     * @return random integer value
     */
    public static int getIntClamp(int top){
        return random.nextInt(top);
    }

    /**
     * Returns an integer in a certain range.
     * @param lower minimum integer value (must be positive)
     * @param upper maximum integer value
     * @return random integer value
     */
    public static int getIntClamp(int lower, int upper){
        int i = getIntClamp(upper);
        while(i < lower){
            i = getIntClamp(upper);
        }
        return i;
    }

    /**
     * @return random boolean value
     */
    public static Boolean getBoolean(){
        return random.nextBoolean();
    }

    /**
     * @return random float value from 0.0 to 1.0
     */
    public static Float getFloat(){
        return random.nextFloat();
    }

    /**
     * @return random double float value
     */
    public static Double getGaussian(){
        return random.nextGaussian();
    }

    /**
     * Return Double Gaussian.
     * @param shift sets the median.
     * @param range sets the size of the of the range for both sides from the middle.
     */
    public static Double getGaussianShifted(double shift, double range){
        return (getGaussian() * range / 2.0D + shift);

    }
}
