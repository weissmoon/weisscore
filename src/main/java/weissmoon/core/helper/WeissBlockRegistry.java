package weissmoon.core.helper;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import weissmoon.core.block.IBlockWeiss;

import java.util.*;

/**
 * Registry of IBlockWeiss instances
 * Created by Weissmoon on 3/22/19.
 */
public class WeissBlockRegistry {

    public static final WeissBlockRegistry weissBlockRegistry = new WeissBlockRegistry();
    private List<Block> blocks = new ArrayList<>();
    private List<BlockItem> blockItems = new ArrayList<>();

    public void regBlock(Block block){
        if(block instanceof IBlockWeiss){
            weissBlockRegistry.blocks.add(block);
            if(((IBlockWeiss) block).hasCustomBlockItem()){
                weissBlockRegistry.blockItems.add(((IBlockWeiss) block).getCustomBlockItem());
            }
        }
    }

    public List<Block>getBlockList(){
        List<Block> list = new ArrayList<>(blocks);
        return list;
    }
}
