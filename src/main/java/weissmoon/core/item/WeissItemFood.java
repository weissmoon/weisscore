package weissmoon.core.item;

import net.minecraft.item.Food;
import net.minecraft.item.ItemGroup;

/**
 * Base item food class
 * automatically register: modID, registry name, unlocalized name, into mod ItemRegistry for model registration
 */
public class WeissItemFood extends WeissItem{

    public WeissItemFood (int point, float sat, boolean wolf, boolean always, boolean fast, String name){
        this(new Properties(), point, sat, wolf, always, fast, name);
    }

    public WeissItemFood (Properties properties, int point, float sat, boolean wolf, boolean always, boolean fast, String name){
        super(name, properties.food(FoodBuilder.buildFood(point, sat, wolf, always, fast)).group(ItemGroup.FOOD));
    }

    private static class FoodBuilder{
        private static Food buildFood(int point, float sat, boolean wolf, boolean always, boolean fast){
            Food.Builder builder = new Food.Builder();
            builder.saturation(point);
            builder.saturation(sat);
            if (wolf) builder.meat();
            if (always) builder.setAlwaysEdible();
            if (fast) builder.fastToEat();
            return builder.build();
        }
    }
}
