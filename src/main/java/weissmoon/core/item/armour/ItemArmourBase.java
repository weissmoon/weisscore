package weissmoon.core.item.armour;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IItemWeiss;

/**
 * Base Armour class
 * automatically register: modID, registry name, unlocalized name, into mod ItemRegistry for model registration
 * Created by Weissmoon on 4/5/19.
 */
public class ItemArmourBase extends ArmorItem implements IItemWeiss{

    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;

    public ItemArmourBase(String name, IArmorMaterial materialIn, EquipmentSlotType equipmentSlotIn){
        this(name, materialIn, equipmentSlotIn, (new Item.Properties()).group(ItemGroup.COMBAT));
    }

    public ItemArmourBase(String name, IArmorMaterial materialIn, EquipmentSlotType equipmentSlotIn, Properties properties){
        super(materialIn, equipmentSlotIn, properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void registerIcons (IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public IIcon getIcon (ItemStack stack){
        return this.itemIconWeiss;
    }

}
