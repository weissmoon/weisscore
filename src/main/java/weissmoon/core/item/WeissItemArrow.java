package weissmoon.core.item;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;

/**
 * Base item arrow class
 * automatically register: modID, registry name, unlocalized name, into mod ItemRegistry for model registration
 * Created by Weissmoon on 1/4/19.
 */
public abstract class WeissItemArrow extends ArrowItem implements IItemWeiss{

    protected IIcon[] itemIconArray;
    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;

    public WeissItemArrow(String name){
        this(name, new Properties());
    }

    public WeissItemArrow(String name, Properties properties){
        super(properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @Override
    public abstract AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter);

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void registerIcons (IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }

    /**
     * Use for icon arrays(Held item)
     */
    @OnlyIn(Dist.CLIENT)
    @Override
    public IIcon getIcon (ItemStack stack){
        return this.itemIconWeiss;
    }

}
