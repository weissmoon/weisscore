package weissmoon.core.item;

import net.minecraft.item.ItemStack;

/**
 * Created by Weissmoon on 1/11/20.
 */
public interface IItemWeissOverride extends IItemWeiss{

    default String getOriginalModID(ItemStack itemStack){
        return getModID();
    }

    default String getCreatorModId(ItemStack itemStack){
        return getTrueModID(itemStack);
    }

    String getTrueModID(ItemStack itemStack);
}
