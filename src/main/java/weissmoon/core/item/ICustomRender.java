package weissmoon.core.item;

/**
 * Created by Weissmoon on 9/29/16.
 * @deprecated use instead {@link weissmoon.core.api.client.item.IItemRenderCustom}
 */
@Deprecated
public interface ICustomRender {
}
