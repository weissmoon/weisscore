package weissmoon.core.item;

import net.minecraft.item.*;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;

/**
 * Base Item class
 * automatically register: modID, registry name, unlocalized name, into mod ItemRegistry for model registration
 */
public abstract class WeissItem extends Item implements IItemWeiss{
    @OnlyIn(Dist.CLIENT)
    protected IIcon[] itemIconArray;
    @OnlyIn(Dist.CLIENT)
    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;

    public WeissItem(String name){
        this(name, new Properties());
    }

    public WeissItem(String name, Properties properties){
        super(properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void registerIcons (IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public boolean hasEffect (ItemStack stack){
        return false;
    }

    /**
     * Use for icon arrays(Held item)
     */
    @OnlyIn(Dist.CLIENT)
    @Override
    public IIcon getIcon (ItemStack stack){
        return this.itemIconWeiss;
    }
}
