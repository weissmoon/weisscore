//package weissmoon.core.item;
//
//import net.minecraft.init.*;
//import net.minecraft.util.ResourceLocation;
//import net.minecraftforge.fml.common.registry.GameRegistry;
//import weissmoon.core.WeissCore;
//
///**
// * Dummy items registered in core mod
// * will deprecate in 1.13+
// */
//public class CoreDummyItems{
//
//    public static final WeissDummy dummyItem = new CoreDummyItem();
//
//    public static final String lamp = "comparingLamp";
//
//    public static void initDummyItems (){
//        dummyItem.addDummyItem(lamp);
//    }
//
//    public static void initRecipes(){
//        GameRegistry.addShapedRecipe(new ResourceLocation("weisscore:comparingLamp"),new ResourceLocation("weisscore"),WeissCore.newDummyItemStack(CoreDummyItems.lamp, 1), "rLg", " t ", 'r', "dyeRed", 'L', Blocks.REDSTONE_LAMP, 'g', "dyeGreen", 't', Items.COMPARATOR);
//    }
//}
