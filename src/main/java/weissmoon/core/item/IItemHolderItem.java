package weissmoon.core.item;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.ITextComponent;
import weissmoon.core.utils.NBTHelper;

import javax.annotation.Nonnull;

/**
 * Created by Weissmoon on 10/1/19.
 */
public interface IItemHolderItem{

    String HOLD_NBT = "holdNBT";

    default ITextComponent getHolderDisplayName(ItemStack itemStack){
        return getHolding(itemStack).getDisplayName();
    }

    static boolean setHolderItem(@Nonnull ItemStack stack,@Nonnull ItemStack plank){
        CompoundNBT cmp = plank.serializeNBT();
        cmp.putByte("Count", (byte)1);
        NBTHelper.setTagCompound(stack, HOLD_NBT, cmp);
        return true;
    }

    static ItemStack getHolding(ItemStack stack){
        if(!NBTHelper.hasTag(stack, HOLD_NBT))
            return ItemStack.EMPTY;
        CompoundNBT cmp = NBTHelper.getTagCompound(stack, HOLD_NBT);
        return ItemStack.read(cmp);
    }
}
