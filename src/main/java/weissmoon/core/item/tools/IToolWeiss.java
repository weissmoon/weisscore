package weissmoon.core.item.tools;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.TieredItem;

/**
 * Used to fix mismatched constructor signatures
 * Created by Weissmoon on 12/8/21.
 */
public interface IToolWeiss<T extends TieredItem>{


    static Multimap<Attribute, AttributeModifier> buildAttributemap(float attackDamage, float attackSpeed){
        ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
        builder.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(Item.ATTACK_DAMAGE_MODIFIER, "Tool modifier", attackDamage, AttributeModifier.Operation.ADDITION));
        builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(Item.ATTACK_SPEED_MODIFIER, "Tool modifier", attackSpeed, AttributeModifier.Operation.ADDITION));
        return builder.build();
    }

    /**
     * Force tools to implement method.
     * @param equipmentSlot apply attributes when in slot
     * @return the map containing itemstack attribute
     */
    default Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlotType equipmentSlot){
        return null;
    }
}
