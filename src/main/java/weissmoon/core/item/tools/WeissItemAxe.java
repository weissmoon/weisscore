package weissmoon.core.item.tools;

import com.google.common.collect.Multimap;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IItemWeiss;

/**
 * Base ItemAxe class
 * automatically register: modID, registry name, unlocalized name, into mod ItemRegistry for model registration
 * Created by Weissmoon on 8/17/19.
 */
public class WeissItemAxe extends AxeItem implements IItemWeiss, IToolWeiss<WeissItemAxe>{

    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;
    private final Multimap<Attribute, AttributeModifier> toolAttributes;

    public WeissItemAxe(IItemTier material, float damage, float speed, String name){
        this(material, damage, speed, (new Item.Properties()).group(ItemGroup.TOOLS), name);
    }

    public WeissItemAxe(IItemTier material, float damage, float speed, Properties properties, String name){
        super(material, 0, 0, properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
        this.toolAttributes = IToolWeiss.buildAttributemap(damage, speed);
    }

    @Override
    public final String getModID(){
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @Override
    public IIcon getIcon(ItemStack stack){
        return this.itemIconWeiss;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }

    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlotType equipmentSlot){
        return equipmentSlot == EquipmentSlotType.MAINHAND ? this.toolAttributes : super.getAttributeModifiers(equipmentSlot);
    }
}
