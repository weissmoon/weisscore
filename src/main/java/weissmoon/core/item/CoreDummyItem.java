//package weissmoon.core.item;
//
//import net.minecraft.item.ItemStack;
//import weissmoon.core.utils.NBTHelper;
//
///**
// * Dummy item class for mod uses only 1 item id will deprecate in 1.13+
// */
//public class CoreDummyItem extends WeissDummy{
//
//    public CoreDummyItem(){
//        super();
//    }
//
//    @Override
//    public ItemStack newDummyItemStack (String name, int size){
//        ItemStack stack = new ItemStack(this, size);
//        if (itemName.contains(name)){
//            NBTHelper.setString(stack, ITEM_NAME_NBT, name);
//        }
//        return stack;
//    }
//}
