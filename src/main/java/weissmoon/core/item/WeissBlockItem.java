package weissmoon.core.item;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.block.IBlockWeiss;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;

/**
 * Base item block class
 * automatically register: modID, registry name, unlocalized name, into mod ItemRegistry for model registration
 * Created by Weissmoon on 3/15/19.
 */
public class WeissBlockItem extends BlockItem implements IBlockItemWeiss{

    private final String ModId;
    protected final String RegName;
    protected IIcon itemIconWeiss;

    public WeissBlockItem(Block block){
        this(block, new Properties());
    }

    public WeissBlockItem(Block block, Properties properties){
        super(block, properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = ((IBlockWeiss)block).getWeissName();
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @Override
    public final String getModID(){
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @Override
    public IIcon getIcon(ItemStack stack){
        return this.itemIconWeiss;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }
}
