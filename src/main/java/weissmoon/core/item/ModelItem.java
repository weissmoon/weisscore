package weissmoon.core.item;

import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;

/**
 * Dummy item used to add models to guis
 * Automatically removed in a ticking inventory
 * Created by Weissmoon on 6/5/21.
 */
public class ModelItem extends Item implements IItemWeiss{

    @OnlyIn(Dist.CLIENT)
    protected IIcon[] itemIconArray;
    @OnlyIn(Dist.CLIENT)
    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;

    public ModelItem(String name){
        this(name, new Item.Properties());
    }

    public ModelItem(String name, Item.Properties properties){
        super(properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @OnlyIn(Dist.CLIENT)
    public void registerIcons (IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public boolean hasEffect (ItemStack stack){
        return false;
    }

    /**
     * Use for icon arrays(Held item)
     */
    @OnlyIn(Dist.CLIENT)
    @Override
    public IIcon getIcon (ItemStack stack){
        return this.itemIconWeiss;
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        //NOOP
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected){
        stack.setCount(0);
        stack = ItemStack.EMPTY;
        if(!stack.isEmpty())
            stack.setCount(0);
    }

    @Override
    public int getEntityLifespan(ItemStack itemStack, World world){
        return 0;
    }
}
