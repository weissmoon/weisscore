package weissmoon.core.item;

import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;

/**
 * Helper interface for Items
 */
public interface IItemWeiss{

    /**
     * @return registry mod ID
     */
    String getModID();

    /**
     * @return registry name
     */
    String getWeissName();

    /**
     * @param stack to get IIcon for
     * @param pass render pass
     *             render pass was removed
     * @return IIcon to render
     */
    @OnlyIn(Dist.CLIENT)
    @Deprecated//wait for 1.15 renderer
    default IIcon getIcon (ItemStack stack, int pass){
        return null;
    }

    /**
     * @param stack to get IIcon for
     * @return IIcon to render
     */
    @OnlyIn(Dist.CLIENT)
    IIcon getIcon (ItemStack stack);

    /**
     * Used to register Block model/StateMapper
     * @param iconRegister instance to call
     */
    @OnlyIn(Dist.CLIENT)
    public void registerIcons (IIconRegister iconRegister);
}
