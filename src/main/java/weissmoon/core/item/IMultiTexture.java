package weissmoon.core.item;


import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.List;

/**
 * Used for item class returning multiple IIcons
 * Created by Weissmoon on 7/22/16.
 */
@Deprecated
public interface IMultiTexture {
    @OnlyIn(Dist.CLIENT)
    public List<String> getTextures();
}
