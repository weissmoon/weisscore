//package weissmoon.core.entity;
//
//import net.minecraft.block.BlockState;
//import net.minecraft.block.material.Material;
//import net.minecraft.entity.Entity;
//import net.minecraft.block.Blocks;
//import net.minecraft.entity.EntityType;
//import net.minecraft.entity.LivingEntity;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.entity.projectile.ProjectileHelper;
//import net.minecraft.entity.projectile.ThrowableEntity;
//import net.minecraft.nbt.CompoundNBT;
//import net.minecraft.particles.ParticleTypes;
//import net.minecraft.util.math.*;
//import net.minecraft.util.math.vector.Vector3d;
//import net.minecraft.world.World;
//import net.minecraftforge.registries.ObjectHolder;
//import weissmoon.core.WeissCore;
//import weissmoon.core.compat.RWBYCompat;
//import weissmoon.core.handler.ConfigurationHandler;
//import weissmoon.core.lib.WeissMods;
//
//import javax.annotation.Nullable;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Optional;
//
///**
// * Created by Weissmoon on 2/28/18.
// */
//public class EntityLinearProjectile extends ThrowableEntity{
//
//    private static final String TAGMOTIONX = "lastMotionX";
//    private static final String TAGMOTIONY = "lastMotionY";
//    private static final String TAGMOTIONZ = "lastMotionZ";
//
//    protected LivingEntity thrower;
//    protected String throwerName;
//
//    /**Collide with liquids */
//    protected RayTraceContext.FluidMode collideLiquid = RayTraceContext.FluidMode.NONE;
//    /** Stun enemies */
//    protected boolean ice = false;
//    private static final String ICESTRING = "ice";
//    protected boolean crystallize = false;
//    /**Item appears in collided player's inventory */
//    protected boolean michaelis = false;
//    /**Water drag*/
//    protected boolean waterDrag = false;
//
//    /**Entity phase list*/
//    protected HashSet<Material> phaseList;
//    /**Phase through certain blocks */
//    protected boolean phase = true;
//    public double motionX;
//    public double motionY;
//    public double motionZ;
//
//    @ObjectHolder("weisscore:linearprojectile")
//    public static EntityType<EntityLinearProjectile> TYPE;//DO NOT USE
//
//    public EntityLinearProjectile(EntityType type, World world) {
//        super(type, world);
//        //setSize(1.0F, 1.0F);
//    }
//
//    public EntityLinearProjectile(EntityType type, World world, LivingEntity entity, boolean reverse) {
//        this(type, world);
//        this.thrower = entity;
////        this.throwerName = entity.getName();
//        double x, y, z;
//        Vector3d position = entity.getPositionVec();
//        x = position.x;
//        y = position.y;
//        z = position.z;
//        if (reverse) {
//            x += MathHelper.cos(this.rotationYaw / 180.0F * 3.1415927F) * 0.2F;
//            z += MathHelper.sin(this.rotationYaw / 180.0F * 3.1415927F) * 0.15F;
//        } else {
//            x -= MathHelper.cos(this.rotationYaw / 180.0F * 3.1415927F) * 0.2F;
//            z -= MathHelper.sin(this.rotationYaw / 180.0F * 3.1415927F) * 0.15F;
//        }
//        y -= 0.10000000149011612D;
//        setLocationAndAngles(x, y + entity.getEyeHeight(), z, entity.rotationYaw, entity.rotationPitch);
//        if (reverse) {
//            this.motionX = (MathHelper.sin(this.rotationYaw / 180.0F * 3.1415927F) * MathHelper.cos(this.rotationPitch / 180.0F * 3.1415927F));
//            this.motionZ = (-MathHelper.cos(this.rotationYaw / 180.0F * 3.1415927F) * MathHelper.cos(this.rotationPitch / 180.0F * 3.1415927F));
//            this.motionY = (MathHelper.sin(this.rotationPitch / 180.0F * 3.1415927F));
//        } else {
//            this.motionX = (-MathHelper.sin(this.rotationYaw / 180.0F * 3.1415927F) * MathHelper.cos(this.rotationPitch / 180.0F * 3.1415927F));
//            this.motionZ = (MathHelper.cos(this.rotationYaw / 180.0F * 3.1415927F) * MathHelper.cos(this.rotationPitch / 180.0F * 3.1415927F));
//            this.motionY = (-MathHelper.sin(this.rotationPitch / 180.0F * 3.1415927F));
//        }
//    }
//
//    @Override
//    protected void registerData() {
//
//    }
//
//    @Override
//    public void readAdditional(CompoundNBT compound) {
//        super.readAdditional(compound);
//        //setTicksExisted(compound.getInteger(TAG_TICKS_EXISTED));
//        //setGravity(compound.getFloat(TAG_GRAVITY));
//
//        double lastMotionX = compound.getDouble(TAGMOTIONX);
//        double lastMotionY = compound.getDouble(TAGMOTIONY);
//        double lastMotionZ = compound.getDouble(TAGMOTIONZ);
//
//        motionX = lastMotionX;
//        motionY = lastMotionY;
//        motionZ = lastMotionZ;
//
////        boolean hasShooter = compound.getBoolean(TAG_HAS_SHOOTER);
////        if(hasShooter) {
////            long most = compound.getLong(TAG_SHOOTER_UUID_MOST);
////            long least = compound.getLong(TAG_SHOOTER_UUID_LEAST);
////            UUID identity = getShooterUUID();
////            if(identity == null || most != identity.getMostSignificantBits() || least != identity.getLeastSignificantBits())
////                shooterIdentity = new UUID(most, least);
////        }
//
//    }
//
//    @Override
//    public void writeAdditional(CompoundNBT compound) {
//        super.writeAdditional(compound);
//        //compound.setInteger(TAG_TICKS_EXISTED, getTicksExisted());
//
//        compound.putDouble(TAGMOTIONX, motionX);
//        compound.putDouble(TAGMOTIONY, motionY);
//        compound.putDouble(TAGMOTIONZ, motionZ);
//
////        UUID identity = getShooterUUID();
////        boolean hasShooter = identity != null;
////        compound.setBoolean(TAG_HAS_SHOOTER, hasShooter);
////        if(hasShooter) {
////            compound.setLong(TAG_SHOOTER_UUID_MOST, identity.getMostSignificantBits());
////            compound.setLong(TAG_SHOOTER_UUID_LEAST, identity.getLeastSignificantBits());
////        }
//    }
//
//    @Override
//    public void tick () {
//        if (position.getY() < 0) {
//            this.remove();
//            return;
//        }
//        lastTickPosX = posX;
//        lastTickPosY = posY;
//        lastTickPosZ = posZ;
//
////        if (throwableShake > 0)
////        {
////            --throwableShake;
////        }
//
//        Vec3d vec3 = new Vec3d(this.posX, this.posY, this.posZ);
//        Vec3d vec31 = new Vec3d(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
//        RayTraceResult movingobjectposition = this.world.rayTraceBlocks(new RayTraceContext(vec3, vec31, RayTraceContext.BlockMode.COLLIDER, collideLiquid, this));
//        vec3 = new Vec3d(this.posX, this.posY, this.posZ);
//        vec31 = new Vec3d(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
//
//        //Phase through some Blocks
//        if (movingobjectposition != null && movingobjectposition.getType() == RayTraceResult.Type.BLOCK){
//            BlockState clollided = this.world.getBlockState(((BlockRayTraceResult) movingobjectposition).getPos());
//            if (this.phase){
//                HashSet<Material> list = this.phaseList;
//                    if (list.contains(clollided.getMaterial())){
//                        WeissCore.proxy.spawnBlockparticles(movingobjectposition, this.dimension, new Vec3d(Math.abs(this.motionX), Math.abs(this.motionY), Math.abs(this.motionZ)), false);
//                        movingobjectposition = null;
//                    }
//            }
//        }
//
//        if (movingobjectposition != null){
//            vec31 = new Vec3d(movingobjectposition.getHitVec().x, movingobjectposition.getHitVec().y, movingobjectposition.getHitVec().z);
//        }
//
//        if (!this.world.isRemote){
//            Entity entity = null;
//            /*addCord renamed expand
//            * expand renamed grow*/
//            List list = this.world.getEntitiesWithinAABBExcludingEntity(this, this.getBoundingBox().expand(this.motionX, this.motionY, this.motionZ).grow(1.0D, 1.0D, 1.0D));
//            //List list = this.world.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
//            double d0 = 0.0D;
//
//            for (int i = 0; i < list.size(); i++){
//                Entity entity1 = (Entity)list.get(i);
//
//                //removed ticks in air
//                //projectile cannot return
//                if (entity1.canBeCollidedWith() && !entity1.isEntityEqual(this.getThrower())){// || (this.ticksInAir >= 25))){
//                    float f = 0.3F;
//                    AxisAlignedBB axisalignedbb = entity1.getBoundingBox().expand(f, f, f);
//                    Optional<Vec3d> movingobjectposition1 = axisalignedbb.rayTrace(vec3, vec31);
//
//                    if (movingobjectposition1 != null){
//                        double d1 = vec3.distanceTo(movingobjectposition1.get());
//
//                        if ((d1 < d0) || (d0 == 0.0D)){
//                            entity = entity1;
//                            d0 = d1;
//                        }
//                    }
//                }
//            }
//
//            if (entity != null){
//                movingobjectposition = new EntityRayTraceResult(entity);
//            }
//
//            //Crystallize
//            if (movingobjectposition != null) {
//                if (WeissMods.instance.isRWBYLoaded() && RWBYCompat.isEntityCrystallizer(movingobjectposition) && (!this.ice) && this.crystallize) {
//                    this.ice = true;
//                    return;
//                }
//            }
//
//            //Michaelis
//            if (movingobjectposition != null && movingobjectposition instanceof EntityRayTraceResult && ((EntityRayTraceResult)movingobjectposition).getEntity() != null && ((EntityRayTraceResult) movingobjectposition).getEntity() instanceof PlayerEntity){
//                PlayerEntity entityplayer = (PlayerEntity)((EntityRayTraceResult) movingobjectposition).getEntity();
//
//                if (entityplayer.abilities.disableDamage || this.getThrower().equals(entityplayer) || ((this.getThrower() instanceof PlayerEntity) && !((PlayerEntity)this.getThrower()).canAttackPlayer(entityplayer))){
//                    movingobjectposition = null;
//                }
//                if ((!ConfigurationHandler.Common.michaelis.get()) && this.michaelis){
//                    this.michaelisColision((EntityRayTraceResult) movingobjectposition);
//                }
//            }
//        }
//
//        if (movingobjectposition != null)
//        {
//            if (movingobjectposition.getType() == RayTraceResult.Type.BLOCK && world.getBlockState(((BlockRayTraceResult)movingobjectposition).getPos()).getBlock() == Blocks.NETHER_PORTAL)
//            {
//                setPortal(((BlockRayTraceResult) movingobjectposition).getPos());
//            }
//            else
//            {
//                onImpact(movingobjectposition);
//            }
//        }
//
//        this.posX += this.motionX;
//        this.posY += this.motionY;
//        this.posZ += this.motionZ;
//
//        //do not rotate towards movement
//
//        float f2 = getMotionFactor();
//        float f4 = getGravityVelocity();
//
//        if (this.waterDrag) {
//            if (isInWater()) {
//                for (int j = 0; j < 4; j++) {
//                    float f3 = 0.25F;
//                    this.world.addParticle(ParticleTypes.BUBBLE, this.posX - this.motionX * f3, this.posY - this.motionY * f3, this.posZ - this.motionZ * f3, this.motionX, this.motionY, this.motionZ);
//                }
//                f2 = getMotionFactor() * getWaterDragFactor();
//            }
//        }
//
//        if (f2 != 1.0F) {
//            this.motionX *= f2;
//            this.motionY *= f2;
//            this.motionZ *= f2;
//        }
//        if (hasNoGravity())
//            this.motionY -= f4;
//    }
//
//    protected void onImpact (RayTraceResult p_70184_1_){
//        remove();
//    }
//
//
//    public LivingEntity getThrower()
//    {
//        if (this.thrower == null && this.throwerName != null && this.throwerName.length() > 0)
//        {
//            //this.thrower = this.world.getPlayerEntityByName(this.throwerName);
//        }
//
//        return this.thrower;
//    }
//
//    public float getMotionFactor (){
//        return 1;
//    }
//
//    public float getWaterDragFactor(){
//        return 0.8F;
//    }
//
//    public float getGravityVelocity(){
//        return 0.03F;
//    }
//
//    /**
//     * return added to inventory success
//     */
//
//    protected boolean michaelisColision(EntityRayTraceResult p_70184_1_){
//        return false;
//    }
//
//}
