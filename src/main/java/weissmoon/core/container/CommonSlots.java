package weissmoon.core.container;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;

/**
 * Created by Weissmoon on 12/10/21.
 */
public class CommonSlots{

    public static class BlockSlot extends Slot{
        public BlockSlot(IInventory inventoryIn, int index, int xPosition, int yPosition){
            super(inventoryIn, index, xPosition, yPosition);
        }

        @Override
        public boolean isItemValid(ItemStack stack){
            return stack.getItem() instanceof BlockItem;
        }
    }

    public static class ItemSlot extends Slot{
        public ItemSlot(IInventory inventoryIn, int index, int xPosition, int yPosition){
            super(inventoryIn, index, xPosition, yPosition);
        }

        @Override
        public boolean isItemValid(ItemStack stack){
            return !(stack.getItem() instanceof BlockItem);
        }
    }

    /**
     * Size limited Slot
     */
    public static class SizeLimitedSlot extends Slot{

        private int slotSize;

        public SizeLimitedSlot(IInventory inventoryIn, int index, int xPosition, int yPosition, int size){
            super(inventoryIn, index, xPosition, yPosition);
            slotSize = Math.min(size, 64);
        }

        @Override
        public int getSlotStackLimit(){
            return slotSize;
        }
    }

    public static class LockedSlot extends Slot{

        public LockedSlot(IInventory inventoryIn, int index, int xPosition, int yPosition){
            super(inventoryIn, index, xPosition, yPosition);
        }

        @Override
        public boolean canTakeStack(PlayerEntity player){
            return false;
        }

        @Override
        public boolean isItemValid(ItemStack stack){
            return false;
        }
    }
}
