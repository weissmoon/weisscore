package weissmoon.core.lib;

import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;

/**
 * Material reference
 * Created by Weissmoon on 10/29/18.
 */
public class BlockMaterial {

    public static final Material UNKNOWN = new Material(MaterialColor.AIR,
            false,
            false,
            false,
            false,
            false,
            true,
            PushReaction.NORMAL);
}
