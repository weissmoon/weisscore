package weissmoon.core.lib;

/**
 * Key enumeration for client/server sync
 * Created by Weissmoon on 11/2/16.
 */
public enum Key {
    UNKNOWN, MODE, PROPEL;

    private Key (){
    }
}
