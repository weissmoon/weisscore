package weissmoon.core.lib;

import net.minecraft.util.ResourceLocation;

/**
 * Core textures used by various mods
 */
public class TexturesCore{
    public static final String TEXTURE_LOCATION = "textures/";
    public static final String ITEM_LOCATION = TEXTURE_LOCATION + "items/";

    public static final ResourceLocation CRYSTAL_ALPHA = new ResourceLocation(ReferenceCore.MOD_ID.toLowerCase(), ITEM_LOCATION + "crystalalpha.png");
    public static final ResourceLocation CRYSTAL_TEXTURE = new ResourceLocation(ReferenceCore.MOD_ID.toLowerCase(), ITEM_LOCATION + "crystalpixel.png");
    public static final ResourceLocation PLATE = new ResourceLocation(ReferenceCore.MOD_ID.toLowerCase(), ITEM_LOCATION + "missingplate.png");
}
