package weissmoon.core.api.registry;

import net.minecraft.item.ItemStack;

/**
 * Sets of armour
 * Class is not in use
 * Created by Weissmoon on 2/15/19.
 */
public class ArmourSet{

    public final ItemStack helm;
    public final ItemStack chest;
    public final ItemStack trousers;
    public final ItemStack feet;

    private final boolean nullItems;

    public ArmourSet(ItemStack helm, ItemStack chest, ItemStack trousers, ItemStack feet){
        this.helm = helm;
        this.chest = chest;
        this.trousers = trousers;
        this.feet = feet;
        this.nullItems =
                helm     == null ||
                chest    == null ||
                trousers == null ||
                feet     == null;
    }

    public boolean hasNullItems(){
        return this.nullItems;
    }
}
