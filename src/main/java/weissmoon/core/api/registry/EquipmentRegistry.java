package weissmoon.core.api.registry;

import com.google.common.collect.Maps;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.IItemTier;

import java.util.HashMap;

/**
 * Class originally used to track all variant of tools of a material
 * Class is not in use
 * Created by Weissmoon on 2/15/19.
 */
public class EquipmentRegistry{

    public static final HashMap<IArmorMaterial, ArmourSet> ArmourSets =  Maps.newHashMap();
    public static final HashMap<IItemTier, Toolset> ToolSet =  Maps.newHashMap();
    public static final HashMap<IItemTier, ModdedTools> ModdedToolSet =  Maps.newHashMap();

    ArmourSet registerArmourSet(IArmorMaterial material, ArmourSet armourSet){
        return armourSet;
    }

    Toolset registerArmourSet(IItemTier material, Toolset toolset){
        return toolset;
    }
}
