package weissmoon.core.api.registry;

import net.minecraft.item.ItemStack;

/**Class intended to contail Vanilla tool item tools sets
 * Class is not in use
 * Created by Weissmoon on 2/15/19.
 */
public class Toolset{

    public final ItemStack shovel;
    public final ItemStack pickaxe;
    public final ItemStack axe;
    public final ItemStack sword;
    public final ItemStack hoe;


    public Toolset(ItemStack shovel, ItemStack pickaxe, ItemStack sword, ItemStack axe, ItemStack hoe){
        this.shovel = shovel;
        this.pickaxe = pickaxe;
        this.sword = sword;
        this.axe = axe;
        this.hoe = hoe;
    }
}
