package weissmoon.core.api.registry;

import net.minecraft.item.Item;
import weissmoon.core.api.client.item.IHUDItem;

/**
 * Registry Class used to register {@link IHUDItem}
 * Created by Weissmoon on 1/5/20.
 */
public class HUDItemRegistry{

    /**
     * Used to attach IHUDItems to Items
     * @param item Item to link IHUDItem to
     * @param ihudItem IHUDItem to link to item
     */
    public void registerIIHUDItem(Item item, IHUDItem ihudItem){}

    /**
     * Can be used to get the IGUDItem for a specific item
     * @param item Item to get IHUDItem to
     * @return returns an instance of IHUDItem, can be null
     */
    public IHUDItem getIHUDItem(Item item){
        return new IHUDItem(){};
    }
}
