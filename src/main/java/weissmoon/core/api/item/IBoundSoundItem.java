package weissmoon.core.api.item;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;

/**
 * A class used to have moving sounds similar to minecarts
 */
public interface IBoundSoundItem{

    /**
     * Is the sound done playing
     * @param heldStack ItemStack currently in the players hand
     * @param player Player the sound is attached to
     * @return true to stop the sound playing
     */
    boolean isSoundDone(ItemStack heldStack, PlayerEntity player);
}
