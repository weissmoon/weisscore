package weissmoon.core.api;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.api.client.render.DummyRenderRegistry;
import weissmoon.core.api.client.render.IRenderRegistry;
import weissmoon.core.api.registry.HUDItemRegistry;

/**
 * Created by Weissmoon on 1/24/18.
 */
public class WeissCoreAPI{


    /**
     * this is the registry that you use to add you own IItemRender objects
     * you can add to it anytime after the construction event and before the postInit event
     * <b>DO NOT OVERRIDE THIS</b>
     * @see IRenderRegistry
     */
    @OnlyIn(Dist.CLIENT)
    public static IRenderRegistry renderRegistry = new DummyRenderRegistry();

    @OnlyIn(Dist.CLIENT)
    public static final IItemRenderer noRenderer = new IItemRenderer() {
        @Override
        public boolean handleRenderType(ItemStack item, ItemCameraTransforms.TransformType cameraTransformType) {
            return false;
        }

        @Override
        public boolean shouldUseRenderHelper(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, ItemRendererHelper helper) {
            return false;
        }

        @Override
        public void renderItem(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, MatrixStack matrixStack, Object... data) {

        }
    };

    /**
     * this is the registry that you use to add you own IHUDItem objects
     * you can add to it anytime after the construction event and before the postInit event
     * <b>DO NOT OVERRIDE THIS</b>
     */
    @OnlyIn(Dist.CLIENT)
    public static HUDItemRegistry hudItemRegistry = new HUDItemRegistry();

    public static final int API_VERSION = 4;
}
