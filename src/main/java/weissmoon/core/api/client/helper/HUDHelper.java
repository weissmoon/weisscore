package weissmoon.core.api.client.helper;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.item.ItemStack;
import net.minecraft.util.HandSide;
import net.minecraft.util.ResourceLocation;

/**
 * Created by Weissmoon on 1/5/20.
 */
public class HUDHelper {

    public static Minecraft minecraft = Minecraft.getInstance();

    public static void renderItemOppositeOffhandHud(MatrixStack matrixStack, ItemStack stack, String count, int textColor){
        HandSide side = minecraft.player.getPrimaryHand().opposite();
        MainWindow sr = minecraft.mainWindow;
        int i = sr.getScaledWidth() / 2;
        minecraft.getTextureManager().bindTexture(new ResourceLocation("textures/gui/widgets.png"));
        if(stack != null){
            if (side == HandSide.RIGHT)
                minecraft.ingameGUI.blit(matrixStack, i - 120, sr.getScaledHeight() - 23, 24, 22, 29, 24);
            else
                minecraft.ingameGUI.blit(matrixStack, i + 91, sr.getScaledHeight() - 23, 53, 22, 29, 24);
        }
        if(stack != null){
            int l1 = sr.getScaledHeight() - 16 - 3;

            int stackPos, stringPos;

            if(side == HandSide.RIGHT){
                stackPos = i - 117;
                stringPos = i - 100;
            }else{
                stackPos = i + 101;
                stringPos = i + 118;
            }
            minecraft.getItemRenderer().renderItemAndEffectIntoGUI(stack, stackPos, l1);
            GlStateManager.disableDepthTest();
            matrixStack.translate(0.0D, 0.0D, 250.0F);
            IRenderTypeBuffer.Impl irendertypebuffer$impl = IRenderTypeBuffer.getImpl(Tessellator.getInstance().getBuffer());
            minecraft.fontRenderer.renderString(count, stringPos - minecraft.fontRenderer.getStringWidth(count), l1 + 6 + 3, textColor, true, matrixStack.getLast().getMatrix(), irendertypebuffer$impl, false, 0, 15728880);
            irendertypebuffer$impl.finish();
            matrixStack.translate(0.0D, 0.0D, -250.0F);
            GlStateManager.enableDepthTest();
        }
    }


    public static void renderItemOppositeOffhandHudNoString(MatrixStack matrixStack, ItemStack stack){
        HandSide side = minecraft.player.getPrimaryHand().opposite();
        MainWindow sr = minecraft.mainWindow;
        int i = sr.getScaledWidth() / 2;
        minecraft.getTextureManager().bindTexture(new ResourceLocation("textures/gui/widgets.png"));
        if(stack != null){
            if(side == HandSide.RIGHT)
                minecraft.ingameGUI.blit(matrixStack, i - 120, sr.getScaledHeight() - 23, 24, 22, 29, 24);
            else
                minecraft.ingameGUI.blit(matrixStack, i + 91, sr.getScaledHeight() - 23, 53, 22, 29, 24);
        }
        if(stack != null){
            int l1 = sr.getScaledHeight() - 16 - 3;

            if(side == HandSide.RIGHT)
                minecraft.getItemRenderer().renderItemAndEffectIntoGUI(stack, i - 117, l1);
            else
                minecraft.getItemRenderer().renderItemAndEffectIntoGUI(stack, i + 101, l1);
            matrixStack.translate(0.0D, 0.0D, -250.0F);
            GlStateManager.enableDepthTest();
        }
    }
}
