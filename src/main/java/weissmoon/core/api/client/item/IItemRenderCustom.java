package weissmoon.core.api.client.item;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * An item must implement this interface to have a custom renderer
 * Created by Weissmoon on 9/29/16.
 */

public interface IItemRenderCustom{

    /**
     * Called during initialization
     * @return the IItemRender instance for the item
     */
    @OnlyIn(Dist.CLIENT)
    IItemRenderer getIItemRender();
}