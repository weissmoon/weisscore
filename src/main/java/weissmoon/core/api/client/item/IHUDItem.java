package weissmoon.core.api.client.item;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.item.ItemStack;

/**
 * Class used for rendering additional hud elements when holding an item
 * Created by Weissmoon on 1/5/20.
 */
public interface IHUDItem{

    /**
     * Is the handler only used to render an ItemStack similar to an off hand item
     * @param heldStack ItemStack currently in the players hand
     * @return true to simply render an ItemStack with the offhand gui element
     */
    default boolean isSimple(ItemStack heldStack){
        return true;
    }

    /**
     * ItemStack being rendered if isSimple() returns true
     * @param heldStack ItemStack currently in the players hand
     * @return the ItemStack to render in the gui if isSimple() returns true
     */
    default ItemStack getDisplayStack(ItemStack heldStack){
        return ItemStack.EMPTY;
    }

    /**
     * Should the handler render the extra elements of the ItemStack being held
     * @param heldStack ItemStack currently in the players hand
     * @return true to render extra elements
     */
    default boolean doRenderHUD(ItemStack heldStack){
        return false;
    }

    /**
     * Render the extra elements of the ItemStack being held
     * @param matrixStack the Matrix Stack being passed in to render
     * @param heldStack ItemStack currently in the players hand
     */
    default void renderHUD(MatrixStack matrixStack, ItemStack heldStack){}

    /**
     * Render the extra elements of the ItemStack previously held
     * @param matrixStack the Matrix Stack being passed in to render
     * @param heldStack ItemStack currently in the players hand
     */
    default void renderPreviousHUD(MatrixStack matrixStack, ItemStack heldStack){
        renderHUD(matrixStack, heldStack);
    }

    /**
     * Animation time in for the current hud type for current item
     * @param itemStack ItemStack currently in the players hand
     */
    default int getMaxTickIn(ItemStack itemStack){
        return 0;
    }

    /**
     * Animation time out for the current hud type for previous item
     * @param itemStack ItemStack currently in the players hand
     */
    default int getMaxTickOut(ItemStack itemStack){
        return 0;
    }
}
