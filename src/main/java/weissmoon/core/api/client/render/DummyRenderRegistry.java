package weissmoon.core.api.client.render;

import net.minecraft.item.Item;
import weissmoon.core.api.client.item.IItemRenderer;

/**
 * Dummy external registry to be used for API
 * Created by Weissmoon on 1/24/18.
 */
public class DummyRenderRegistry implements IRenderRegistry{

    @Override
    public void registerItemRenderer(Item item, IItemRenderer renderer){}

    @Override
    public IItemRenderer getIItemRenderer(Item item){
        return null;
    }
}
