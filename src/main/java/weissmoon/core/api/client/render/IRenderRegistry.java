package weissmoon.core.api.client.render;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import weissmoon.core.api.client.item.IItemRenderer;

/**
 * Created by Weissmoon on 1/24/18.
 */
public interface IRenderRegistry{

    void registerItemRenderer(Item item, IItemRenderer renderer);

    IItemRenderer getIItemRenderer(Item item);

    default IItemRenderer getIItemRenderer(ItemStack itemStack){
        return getIItemRenderer(itemStack.getItem());
    }

}
