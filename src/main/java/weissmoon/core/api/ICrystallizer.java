package weissmoon.core.api;

//A Stirling class
/**
 * Entity or block that crystallizes
 * Class used to crystallize bullets that collide with Entity or Blocks
 */
public interface ICrystallizer{}