package weissmoon.core.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.network.NetworkEvent;
import org.apache.commons.lang3.Validate;
import net.minecraft.client.Minecraft;
import weissmoon.core.WeissCore;

import java.util.UUID;
import java.util.function.Supplier;

/**
 * Bound sound network packet (server -> client)
 */
public class BoundSoundPacket{
    private String player;
    private String soundName;
    private String category;
    private float loudness;
    private float pitch;

    public BoundSoundPacket(PlayerEntity entityPlayer, SoundCategory category, String soundName, float loudness, float pitch){
        if ((entityPlayer != null)){
            Validate.notNull(soundName, "boundSoundName");
            this.player = entityPlayer.getUniqueID().toString();
            this.soundName = soundName;
            this.category = category.getName();
            this.loudness = loudness;
            this.pitch = pitch;
        }
    }

    public void fromBytes (ByteBuf buf){
        int length = buf.readShort();
        this.player = buf.readBytes(length).toString();
        length = buf.readShort();
        this.soundName = buf.readBytes(length).toString();
        length = buf.readShort();
        this.category = buf.readBytes(length).toString();
        this.loudness = buf.readFloat();
        this.pitch = buf.readFloat();
    }

    public void toBytes (ByteBuf buf){
        String player1 = this.player;
        String sound = this.soundName;
        String category = this.category;
        NetworkHelper.writeString(player1, buf);
        NetworkHelper.writeString(sound, buf);
        NetworkHelper.writeString(category, buf);
        buf.writeFloat(this.loudness);
        buf.writeFloat(this.pitch);
    }

    public void onMessage (BoundSoundPacket message, Supplier<NetworkEvent.Context> ctx){
        if (ctx.get().getDirection().getReceptionSide() == LogicalSide.CLIENT){
            PlayerEntity s = Minecraft.getInstance().world.getPlayerByUuid(UUID.fromString(this.player));
            SoundCategory vat = SoundCategory.MASTER;
            for(SoundCategory value:SoundCategory.values()){
                value.getName().matches(this.category);
                vat = value;
            }
            WeissCore.proxy.playBoundSound(this.soundName, vat, s, this.loudness, this.pitch);
        }
    }
}
