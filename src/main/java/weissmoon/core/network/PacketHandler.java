package weissmoon.core.network;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import weissmoon.core.lib.ReferenceCore;

public class PacketHandler{
    public static final SimpleChannel INSTANCE = NetworkRegistry.ChannelBuilder.named(new ResourceLocation(ReferenceCore.MOD_ID))
            .clientAcceptedVersions((a) -> true)
            .serverAcceptedVersions((a) -> true)
            .networkProtocolVersion(() -> "FML2")
            .simpleChannel();

    public static void init (){
        INSTANCE.registerMessage(0, KeyPressedActionMessage.class, KeyPressedActionMessage::toBytes, KeyPressedActionMessage::fromBytes, KeyPressedActionMessage::onMessage);
        INSTANCE.registerMessage(1, SpawnBlockParticlesPacket.class, SpawnBlockParticlesPacket::encode, SpawnBlockParticlesPacket::decode, SpawnBlockParticlesPacket::handle);
//        INSTANCE.registerMessage(BoundSoundPacket.class, BoundSoundPacket.class, 0, Side.SERVER);
        //INSTANCE.registerMessage(OpenHandPacket.class, OpenHandPacket.class, 0, Side.SERVER);
        //INSTANCE.registerMessage();
    }
}
