package weissmoon.core.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraftforge.fml.network.NetworkEvent;
import weissmoon.core.api.item.IModesCore;
import weissmoon.core.control.KeyInputMap;
import weissmoon.core.lib.Key;

import java.util.function.Supplier;

/**
 * Key press network packet (client -> server)
 */
public class KeyPressedActionMessage{
    static private byte keyPress;
    static private boolean pressed;

    public KeyPressedActionMessage (){
    }

    public KeyPressedActionMessage (Key key){
        if (key == Key.MODE){
            this.keyPress = ((byte)Key.MODE.ordinal());
        }
    }

    public KeyPressedActionMessage (Key key, boolean pressed){
        if (key == Key.PROPEL){
            this.keyPress = ((byte)Key.PROPEL.ordinal());
            this.pressed = pressed;
        }
    }

    public static KeyPressedActionMessage fromBytes (ByteBuf buf){
        keyPress = buf.readByte();
        pressed = buf.readBoolean();
        return new KeyPressedActionMessage();
    }

    public static void toBytes (KeyPressedActionMessage message, ByteBuf buf){
        buf.writeByte(keyPress);
        buf.writeBoolean(pressed);
    }

    public static void onMessage (KeyPressedActionMessage message, Supplier<NetworkEvent.Context> context){
        if (message.keyPress == Key.UNKNOWN.ordinal()){
            return;
        }

        PlayerEntity entityPlayer = context.get().getSender();

        if (message.keyPress == Key.PROPEL.ordinal()){
            KeyInputMap.getInputMapFor(entityPlayer.getName().getString()).propelKey = message.pressed;
            return;
        }

        if (entityPlayer != null && entityPlayer.getHeldItem(Hand.MAIN_HAND) != ItemStack.EMPTY)
            if (message.keyPress == Key.MODE.ordinal())
                if ((entityPlayer.getHeldItem(Hand.MAIN_HAND).getItem() instanceof IModesCore))
                    ((IModesCore)entityPlayer.getHeldItem(Hand.MAIN_HAND).getItem()).changeToolMode(entityPlayer.getHeldItem(Hand.MAIN_HAND));
    }
}
