package weissmoon.core.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

/**
 * Empty hand packet
 * Not in use not supported by Minecraft
 */
public class OpenHandPacket{

    private String player;

    public OpenHandPacket(){}

    public OpenHandPacket(String playe){
        this.player = playe;
    }

    public void fromBytes (ByteBuf buf){
        int length = buf.readableBytes();
        this.player = buf.readBytes(length).toString();
    }

    public void toBytes (ByteBuf buf){
        String player1 = player;
        NetworkHelper.writeString(player1, buf);
    }

    public void onMessage (OpenHandPacket message, Supplier<NetworkEvent.Context> ctx){
        if (ctx.get().getDirection().getReceptionSide() == LogicalSide.SERVER){
            //ctx.getServerHandler().playerEntity.inventory.currentItem = 0;
        }
    }
}
