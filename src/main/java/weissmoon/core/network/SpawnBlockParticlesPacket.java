package weissmoon.core.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

/**
 * Hit particles network packet (server -> client)
 * Not in use
 */
public class SpawnBlockParticlesPacket{

    static int posX;
    static int posY;
    static int posZ;
    static int dimID;
    static public double xCoord;
    static public double yCoord;
    static public double zCoord;
    static double xVel;
    static double yVel;
    static double zVel;
    static boolean fancy;

    public static SpawnBlockParticlesPacket decode(ByteBuf buf){
        posX = buf.readInt();
        posY = buf.readInt();
        posZ = buf.readInt();
        dimID = buf.readInt();
        xCoord = buf.readDouble();
        yCoord = buf.readDouble();
        zCoord = buf.readDouble();
        xVel = buf.readDouble();
        yVel = buf.readDouble();
        zVel = buf.readDouble();
        fancy = buf.readBoolean();
        return new SpawnBlockParticlesPacket();
    }

    public static void encode(SpawnBlockParticlesPacket packet, PacketBuffer buf){
        buf.writeInt(posX);
        buf.writeInt(posY);
        buf.writeInt(posZ);
        buf.writeInt(dimID);
        buf.writeDouble(xCoord);
        buf.writeDouble(yCoord);
        buf.writeDouble(zCoord);
        buf.writeDouble(xVel);
        buf.writeDouble(yVel);
        buf.writeDouble(zVel);
        buf.writeBoolean(fancy);
    }

    public static void handle(SpawnBlockParticlesPacket message, Supplier<NetworkEvent.Context> ctx){
        if(ctx.get().getDirection().getReceptionSide() == LogicalSide.CLIENT) {
//            RayTraceResult movingobjectposition = Minecraft.getInstance().player.world.
//                    rayTraceBlocks(new Vec3d(
//                            this.xCoord - this.xVel,
//                            this.yCoord - this.yVel,
//                            this.zCoord - this.zVel
//                    ), new Vec3d(
//                            this.xCoord + this.xVel,
//                            this.yCoord + this.yVel,
//                            this.zCoord + this.zVel
//                    ));
//            WeissCore.proxy.spawnBlockparticles(movingobjectposition, dimID, new Vec3d(xVel, yVel, zVel), fancy);
        }
    }
}
