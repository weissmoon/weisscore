package weissmoon.core;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.*;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.*;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.client.render.renderOverride.PublicRenderRegistry;
import weissmoon.core.command.TogglePVP;
import weissmoon.core.event.EntityDrop;
import weissmoon.core.event.EquiptArmourEvent;
import weissmoon.core.handler.ConfigurationHandler;
import weissmoon.core.handler.HUDItemRealRegistry;
import weissmoon.core.lib.*;
import weissmoon.core.network.PacketHandler;
import weissmoon.core.proxy.ClientProxy;
import weissmoon.core.proxy.CommonProxy;
import weissmoon.core.proxy.ServerProxy;

import java.io.File;
import java.util.stream.Collectors;


@Mod(ReferenceCore.MOD_ID)//, name = ReferenceCore.MOD_NAME, version = ReferenceCore.VERSION, guiFactory = ReferenceCore.GUI_FACTORY_CLASS, acceptableRemoteVersions = "*")
public class WeissCore{
    //@Instance(ReferenceCore.MOD_ID)
    public static WeissCore instance;
    public static Logger logger = LogManager.getLogger("WeissCore");
//
//    //@SidedProxy(clientSide = "weissmoon.core.proxy.ClientProxy", serverSide = "weissmoon.core.proxy.ServerProxy")
    public static CommonProxy proxy;

    /**Modules*/
    /**Weisscore Dummy item*/
    private boolean dummyInitialized = false;
    public void setDummyItem (){
        instance.dummyInitialized = true;
    }
    /**Keyhandler and network*/
    private boolean keyHandler = false;
    public void initKeyHandler (){
        instance.keyHandler = true;
    }

    private boolean dummyCoreInitialized = false;

    public WeissCore(){
        instance = this;
        proxy = DistExecutor.runForDist(() -> ClientProxy::new, () -> ServerProxy::new);
        proxy.initAPIRender();
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ConfigurationHandler.COMMON_SPEC);
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, ConfigurationHandler.CLIENT_SPEC);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        MinecraftForge.EVENT_BUS.addGenericListener(Item.class, this::registerItems);

    }

    public void setup (FMLCommonSetupEvent event){
        //ConfigurationHandler.init(new File(event.getModConfigurationDirectory().getAbsolutePath() + File.separator + ReferenceCore.AUTHOR + File.separator + ReferenceCore.MOD_ID + ".cfg"));
        MinecraftForge.EVENT_BUS.register(this);
        if (instance.keyHandler) {
            PacketHandler.init();
        }
        MinecraftForge.EVENT_BUS.register(new EntityDrop());
        MinecraftForge.EVENT_BUS.register(new EquiptArmourEvent());
        proxy.registerEventHandler();
        if (instance.keyHandler)
            proxy.registerKeyHandler();
        proxy.initIcons();
        proxy.initRenderer();
        if(!ModList.get().isLoaded("weissrandom") && ConfigurationHandler.Client.arrowHUD.get())
            HUDItemRealRegistry.registerCore();
    }

    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> evt) {
//        if (instance.dummyInitialized){
////            IForgeRegistry registry = evt.getRegistry();
////            registry.register(CoreDummyItems.dummyItem);
////            CoreDummyItems.initDummyItems();
//        }
    }


    @SubscribeEvent
    public void serverStarting (RegisterCommandsEvent evt){
        TogglePVP.register(evt.getDispatcher());
    }

    public static ItemStack newDummyItemStack (String name){
        return ItemStack.EMPTY;
        //return CoreDummyItems.dummyItem.newDummyItemStack(name, 1);
    }

    public static ItemStack newDummyItemStack (String name, int size){
        return ItemStack.EMPTY;
        //return CoreDummyItems.dummyItem.newDummyItemStack(name, size);
    }

}