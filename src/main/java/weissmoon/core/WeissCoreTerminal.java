package weissmoon.core;

//import weissmoon.core.handler.OreDictinaryCondense;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.*;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLLoader;
import net.minecraftforge.registries.ObjectHolder;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.lib.ReferenceCore;
import weissmoon.core.lib.Strings;
import weissmoon.core.proxy.ClientProxy;

/**
 * Terminal mod used to register IItemRenders and item models
 */
@Mod.EventBusSubscriber
@Mod(ReferenceCore.MOD_ID+"terminal")//, name = ReferenceCore.MOD_NAME+"terminal", version = ReferenceCore.VERSION, dependencies ="after:*", acceptableRemoteVersions = "*")// "required-after:Forge@[14.23.0.2491,);after:*")
public class WeissCoreTerminal{

    public WeissCoreTerminal(){
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::preInit);
    }

    public void preInit(FMLCommonSetupEvent event){
        //OreDictinaryCondense.init(new File(event.getModConfigurationDirectory().getAbsolutePath() + File.separator + ReferenceCore.AUTHOR + File.separator + "OreCondense.cfg"),
                                           //event.getModConfigurationDirectory().getAbsoluteFile() + File.separator + ReferenceCore.AUTHOR + File.separator + "OreCondense" + File.separator);

        //GameRegistry.register(new TestItem());
        MinecraftForge.EVENT_BUS.register(WeissCore.proxy);
        MinecraftForge.EVENT_BUS.register(this);
        if(FMLLoader.getDist() == Dist.CLIENT){
            String clname = WeissCoreAPI.renderRegistry.getClass().getName();
            String expect = "weissmoon.core.client.render.renderOverride.PublicRenderRegistry";
            if(!clname.equals(expect)){
                new IllegalAccessError("The WeissCoreAPI public render registry has been overriden. "
                        +"This will cause crashes, which is why it's labeled "
                        +" \"Do not Override\". Go back to primary school and learn to read. "
                        +"(Expected classname: "+expect+", Actual classname: "+clname+")").printStackTrace();
//              FMLCommonHandler.instance().exitJava(1, true);
                System.exit(1);
            }
            String cliname = WeissCoreAPI.hudItemRegistry.getClass().getName();
            String expectI = "weissmoon.core.handler.HUDItemRealRegistry";
            if(!cliname.equals(expectI)){
                new IllegalAccessError("The WeissCoreAPI public item hud render registry has been overriden. "
                        +"This will cause crashes, which is why it's labeled "
                        +" \"Do not Override\". Go back to primary school and learn to read. "
                        +"(Expected classname: "+expectI+", Actual classname: "+cliname+")").printStackTrace();
//              FMLCommonHandler.instance().exitJava(1, true);
                System.exit(1);
            }
        }
    }

    @Mod.EventBusSubscriber(modid = ReferenceCore.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
    @ObjectHolder(ReferenceCore.MOD_ID)
    public static final class RegistryEvents{

        //@SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> evt){
            //IForgeRegistry<Item> registry = evt.getRegistry();
            //registry.register(new TestItem());

        }

        @OnlyIn(Dist.CLIENT)
        @SubscribeEvent
        public static void registerModels(ModelRegistryEvent event){
            IIconRegister.INSTANCE.exnste();
        }

        @OnlyIn(Dist.CLIENT)
        @SubscribeEvent
        public static void registerBakedModels(ModelBakeEvent event){
            WeissCore.proxy.registerRenderOverride();
            event.getModelRegistry().put(new ModelResourceLocation(Strings.RESOURCE_PREFIX + "renderoverride", "inventory"), ClientProxy.dustBa);
        }
    }
}
