package weissmoon.core.event;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraftforge.event.entity.living.LivingEquipmentChangeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * Created by Weissmoon on 8/20/19.
 */
public class EquiptArmourEvent {

    @SubscribeEvent
    public void onEquiptItem(LivingEquipmentChangeEvent event){
        if(event.getEntityLiving() instanceof PlayerEntity){
            PlayerEntity player = (PlayerEntity)event.getEntityLiving();
            if(player.abilities.isCreativeMode)
                return;
        }
        if(event.getSlot().getSlotType() == EquipmentSlotType.Group.ARMOR){
            ItemStack stack = event.getTo();
            if(stack.hasTag()){
                if(!stack.getTag().contains("WMShowCurse")){
                    ListNBT nbttaglist = stack.getEnchantmentTagList();

                    for(int j = 0; j < nbttaglist.size(); ++j){
                        CompoundNBT nbttagcompound = nbttaglist.getCompound(j);
                        String k = nbttagcompound.getString("id");
                        if(k.equals("minecraft:binding_curse"))
                            stack.getTag().putBoolean("WMShowCurse", true);
                    }
                }
            }
        }
    }
}
