package weissmoon.core.event;

import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.core.helper.RNGHelper;

public class EntityDrop{
    @SubscribeEvent
    public void entityDrops(LivingDropsEvent event){
        if (((event.getEntityLiving() instanceof IMob)) &&
                ((event.getEntityLiving() instanceof net.minecraft.entity.monster.SpiderEntity)) &&
                (RNGHelper.getFloat() > 0.99D)){
            ItemStack itemStack = new ItemStack(Items.STRING);
            itemStack.addEnchantment(Enchantments.SILK_TOUCH, 1);
            event.getDrops().add(new ItemEntity(event.getEntityLiving().world, event.getEntityLiving().getPositionVec().x, event.getEntityLiving().getPositionVec().y, event.getEntityLiving().getPositionVec().z, itemStack));
        }
    }
}
