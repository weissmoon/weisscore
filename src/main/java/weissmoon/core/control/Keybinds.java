package weissmoon.core.control;

import net.minecraft.client.settings.KeyBinding;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_CONTROL;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_M;

/**
 * Keybind reference class
 */
public class Keybinds{
    //public static KeyBinding openslot = new KeyBinding("key.hotbar.0", Keyboard.KEY_0, "key.categories.inventory");
    //key.categories.gameplay
    public static KeyBinding mode = new KeyBinding("weisscore.key.mode", GLFW_KEY_M, "key.category.weisscore");
    public static KeyBinding propel = new KeyBinding("weisscore.key.propel", GLFW_KEY_LEFT_CONTROL, "key.category.weisscore");
}
