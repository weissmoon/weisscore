package weissmoon.core.control;

import net.minecraft.util.Hand;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import weissmoon.core.api.item.IModesCore;
import weissmoon.core.lib.Key;
import weissmoon.core.network.*;
import weissmoon.core.network.PacketHandler;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;

@OnlyIn(Dist.CLIENT)
public class KeyInputHandler{

    int eventKeyID;

    private Key getPressedKeybinding(){
        if (Keybinds.mode.isPressed()){
            return Key.MODE;
        }
        return Key.UNKNOWN;
    }

    private Key getToggleKeybinding(){
        //int eventKey = Keyboard.getEventKey();
        if (eventKeyID == Keybinds.propel.getKey().getKeyCode()){
            return Key.PROPEL;
        }
        return Key.UNKNOWN;
    }

    public boolean isWeissKey(){
        //int eventKey = Keyboard.getEventKey();
        //if (eventKey == Keybinds.openslot.getKeyCode()){
            //return true;
        //}
        if (eventKeyID == Keybinds.propel.getKey().getKeyCode()){
            return true;
        }
        if (eventKeyID == Keybinds.mode.getKey().getKeyCode()){
            return true;
        }
        return false;
    }

    @SubscribeEvent
    public void handleKeyInputEvent(InputEvent.KeyInputEvent event){
        eventKeyID = event.getKey();
        if (!isWeissKey()){
            return;
        }

        Key toggleKeybinding = getToggleKeybinding();

        if (Minecraft.getInstance().isGameFocused()){
            if (Minecraft.getInstance().player != null){
                PlayerEntity entityPlayer = Minecraft.getInstance().player;
                PacketHandler.INSTANCE.sendToServer(new OpenHandPacket(Minecraft.getInstance().player.getName().getString()));

                if (entityPlayer.getHeldItem(Hand.MAIN_HAND) != ItemStack.EMPTY){
                    boolean pressed = event.getAction() == GLFW_PRESS;


                    if ((Minecraft.getInstance().player != null) && (toggleKeybinding == Key.PROPEL)){
                        PacketHandler.INSTANCE.sendToServer(new KeyPressedActionMessage(toggleKeybinding, pressed));
                        KeyInputMap.getInputMapFor(entityPlayer.getName().getString()).propelKey = pressed;
                    }


                    Key pressedKeybinding = getPressedKeybinding();

                    if (pressedKeybinding == Key.UNKNOWN){
                        return;
                    }

                    ItemStack currentlyEquippedItemStack = entityPlayer.getHeldItem(Hand.MAIN_HAND);

                    if (currentlyEquippedItemStack.getItem() instanceof IModesCore){
                        if (entityPlayer.world.isRemote){
                            PacketHandler.INSTANCE.sendToServer(new KeyPressedActionMessage(pressedKeybinding));
                        }
                    }
                }
            }
        }
    }
}