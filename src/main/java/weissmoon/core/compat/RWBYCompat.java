package weissmoon.core.compat;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import weissmoon.core.api.ICrystallizer;

//TODO move to api
public class RWBYCompat{
    public static boolean isEntityCrystallizer(RayTraceResult movingObjectPosition){
        return (movingObjectPosition.getType() == RayTraceResult.Type.ENTITY && ((EntityRayTraceResult)movingObjectPosition).getEntity() instanceof ICrystallizer);
    }

    public static boolean isIceGlyph(Entity entity){
        return (entity instanceof ICrystallizer);
    }
}