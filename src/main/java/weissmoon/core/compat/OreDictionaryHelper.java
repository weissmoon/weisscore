package weissmoon.core.compat;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ForgeTagHandler;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class OreDictionaryHelper{

    @Deprecated
    public static ResourceLocation getOreName(ItemStack stack){
        ResourceLocation[] array = (ResourceLocation[]) ItemTags.getCollection().getOwningTags(stack.getItem()).toArray();
        if (0 < array.length){
            return array[0];
//            return OreDictionary.getOreName(e);
        }

        return null;
    }

    public static boolean isItemOre(ItemStack stack, ResourceLocation tagName){
//        ResourceLocation tagName = new ResourceLocation(oreName);
        if (stack == null || stack == ItemStack.EMPTY || stack.getItem() == Items.AIR){
            return false;
        }
        List<ResourceLocation> list = (List<ResourceLocation>) ItemTags.getCollection().getOwningTags(stack.getItem());
        for(ResourceLocation e : list){
            if (tagName.equals(e))
                return true;
        }
        return false;
    }

    public static boolean oreExists (String name){
        ResourceLocation tagName = new ResourceLocation(name);
        return ItemTags.getCollection().get(tagName) != null;
    }
}
