package weissmoon.core.command;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.command.*;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TranslationTextComponent;

import java.lang.ref.WeakReference;

/**
 * Toggles PVP boolean.
 */
public class TogglePVP {
    private WeakReference<MinecraftServer> server;
//
//    public TogglePVP (MinecraftServer server){
//        this.server = new WeakReference(server);
//    }
//
//    public String getName (){
//        return "togglepvp";
//    }
//
//    public String getUsage (ICommandSender p_71518_1_){
//        return null;
//    }
//
//    @Override
//    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
//        boolean pvp = server.isPVPEnabled();
//        server.setAllowPvp(!pvp);
//        sender.sendMessage(new TextComponentTranslation("weisscore.commands.pvp.success", !pvp));
//    }
//
//    public int getRequiredPermissionLevel (){
//        return 2;
//    }
//
//    public void processCommand (ICommandSender p_71515_1_, String[] p_71515_2_){
//        //boolean pvp = MinecraftServer.getServer().isPVPEnabled();
//        //MinecraftServer.getServer().setAllowPvp(!pvp);
//        //p_71515_1_.addChatMessage(new ChatComponentTranslation("weisscore.commands.pvp.success", new Object[]{Boolean.valueOf(!pvp)}));
//    }

    public static void register(CommandDispatcher<CommandSource> dispatcher) {
        dispatcher.register((LiteralArgumentBuilder)((LiteralArgumentBuilder)Commands.literal("togglepvp").requires((p_198727_0_) -> {
            return p_198727_0_.hasPermissionLevel(4);
        })).executes((sender) -> {
            boolean pvp = ((CommandSource)sender.getSource()).getServer().isPVPEnabled();
            ((CommandSource)sender.getSource()).getServer().setAllowPvp(!pvp);
            ((CommandSource)sender.getSource()).sendFeedback(new TranslationTextComponent("weisscore.commands.pvp.success", !pvp), true);
            return 1;
        }));
    }
}
