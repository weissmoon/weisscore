package weissmoon.core.test;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.*;
import net.minecraft.client.renderer.texture.AtlasTexture;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import org.lwjgl.opengl.GL11;
import weissmoon.core.client.render.WeissRenderItem;
import weissmoon.core.proxy.ClientProxy;

import javax.annotation.Nonnull;

/**
 * Test IItemRender model
 * Created by Weissmoon on 5/22/17.
 */
public class MelonHammerModel extends Model {

    ModelRenderer Shape1;

    public MelonHammerModel() {
        super(RenderType::getEntitySolid);
        this.textureWidth = 4;
        this.textureHeight = 2;


        this.Shape1 = new ModelRenderer(this, 0, 0);
        this.Shape1.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
        this.Shape1.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape1.setTextureSize(4, 2);
        this.Shape1.mirror = true;
        setRotation(this.Shape1, 0.0F, 0.0F, 0.0F);
    }

    public void render(@Nonnull MatrixStack matrixStack, @Nonnull IVertexBuilder buffer, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha){
        GL11.glPushMatrix();
        this.Shape1.render(matrixStack, buffer, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        IBakedModel melon = Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getModel(new ModelResourceLocation("minecraft:melon#inventory"));
        //FMLClientHandler.instance().getClient().renderEngine.bindTexture(new ResourceLocation(melon.getParticleTexture().getIconName()));
        Minecraft.getInstance().textureManager.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
        ClientProxy.weissRenderItem.renderModel(Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getModel(new ModelResourceLocation(
                        "minecraft:melon#inventory")), -1,
                //new ItemStack(Blocks.MELON));
                null);
        GL11.glPopMatrix();
    }


    private void setRotation (ModelRenderer model, float x, float y, float z){
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

}
