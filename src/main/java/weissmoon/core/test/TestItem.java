package weissmoon.core.test;

import net.minecraft.item.ItemStack;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.item.WeissItem;

/**
 * Test Item IItemRendere
 * Created by Weissmoon on 5/24/17.
 */
public class TestItem extends WeissItem implements IItemRenderCustom {
    public TestItem() {
        super("test", new Properties());
    }

    @Override
    public IItemRenderer getIItemRender() {
        return new MelonHammerRenderer();
    }
}
