package weissmoon.core.handler;

import net.minecraftforge.common.ForgeConfig;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.*;
import net.minecraftforge.eventbus.api.SubscribeEvent;
//import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.commons.lang3.tuple.Pair;
import weissmoon.core.lib.ReferenceCore;

import java.io.File;


public class ConfigurationHandler{

    public static final Client CLIENT;
    public static final ForgeConfigSpec CLIENT_SPEC;
    static {
        final Pair<Client, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Client::new);
        CLIENT_SPEC = specPair.getRight();
        CLIENT = specPair.getLeft();
    }

    public static final Common COMMON;
    public static final ForgeConfigSpec COMMON_SPEC;
    static {
        final Pair<Common, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Common::new);
        COMMON_SPEC = specPair.getRight();
        COMMON = specPair.getLeft();
    }

    @Mod.EventBusSubscriber(modid = ReferenceCore.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
//    @ObjectHolder(ReferenceCore.MOD_ID)
    public static class Client {
        public static ForgeConfigSpec.Builder configuration;
        public static BooleanValue lore;
        public static BooleanValue materialEvent;
        public static BooleanValue wolfTip;
        public static BooleanValue extraFancy;
        public static BooleanValue stealthBinding;
        public static BooleanValue arrowHUD;
        public static String CATEGORY_TOOLTIP = "tooltip";
        public static String CATEGORY_RENDER = "render";

        public Client(ForgeConfigSpec.Builder builder) {
            configuration = builder;
            configuration.comment("Common configuration settings")
                    .push("general");
            loadCofiguration();
        }

//        @SubscribeEvent
//        public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event) {
//            if (event.getModID().equalsIgnoreCase(ReferenceCore.MOD_ID)) {
//                loadCofiguration();
//            }
//        }

        private static void loadCofiguration() {
            //general
            lore = configuration.comment("Global Lore setting").define("lore", true);
            arrowHUD = configuration.comment("Shows arrows available when holding a bow").define("arrowHUD", true);

            //render
            extraFancy = configuration.comment("Render some items with extra fancy render").define("extraFancy", true);

            //tooltip
            materialEvent = configuration.comment("Show block material when holding \"Shift\"").define("blockMaterial", true);
            wolfTip = configuration.comment("Show wolves favorite foods").define("wolfTip", true);
            stealthBinding = configuration.comment("Do not show Curse of Binding in tooltip until Equipped").define("stealthBinding", true);
            configuration.pop();
        }
    }

    @Mod.EventBusSubscriber(modid = ReferenceCore.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
//    @ObjectHolder(ReferenceCore.MOD_ID)
    public static class Common {
        public static ForgeConfigSpec.Builder configuration;
        public static final String NEW_LINE = System.getProperty("line.separator");

        public static BooleanValue michaelis;

        public static String CATEGORY_GENERAL = "general";

//        public static void init(File configFile) {
//            if (configuration == null) {
//                configuration = new Configuration(configFile);
//                loadCofiguration();
//            }
//        }

        public Common(ForgeConfigSpec.Builder builder) {
            configuration = builder;
            configuration.comment("Common configuration settings")
                    .push("general");
            loadCofiguration();
        }

//        @SubscribeEvent
//        public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event) {
//            if (event.getModID().equalsIgnoreCase(ReferenceCore.MOD_ID)) {
//                loadCofiguration();
//            }
//        }

        private static void loadCofiguration() {
            //general
            michaelis = configuration.comment("Certain bullets appear in a shot player's inventory if PVP is disabled" + NEW_LINE + "Otherwise bullets phase through").define("michaelis", false);

            configuration.pop();
//            if (configuration.hasChanged()) {
//                configuration.save();
//            }
        }
    }
}