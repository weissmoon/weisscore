package weissmoon.core.handler;

import com.google.common.collect.Maps;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.util.HandSide;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.api.client.item.IHUDItem;
import weissmoon.core.api.registry.HUDItemRegistry;
import weissmoon.core.api.client.helper.HUDHelper;

import java.util.IdentityHashMap;

/**
 * Created by Weissmoon on 1/5/20.
 */
public class HUDItemRealRegistry extends HUDItemRegistry {

    private static final IdentityHashMap<Item, IHUDItem> iHudItems = Maps.newIdentityHashMap();

    public void registerIIHUDItem(Item item, IHUDItem ihudItem){
        if(iHudItems.get(item) == null)
            iHudItems.put(item, ihudItem);
        else{
            System.out.println("Item " + item + " has already been registered");
            System.out.println("Will not register IHUDItem " + ihudItem + " to item " + item);
        }
    }

    public IHUDItem getIHUDItem(Item item){
        if(item instanceof IHUDItem)
            return (IHUDItem) item;
        return iHudItems.getOrDefault(item, null);
    }

    public static void registerCore(){
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(Items.BOW, new IHUDItem() {

            @Override
            public boolean isSimple(ItemStack stack){
                return false;
            }

            @Override
            public boolean doRenderHUD(ItemStack stack){
                PlayerEntity player = Minecraft.getInstance().player;
                for (int i = 0; i < player.inventory.getSizeInventory(); ++i)
                {
                    ItemStack itemstack = player.inventory.getStackInSlot(i);

                    if (itemstack.getItem() instanceof ArrowItem)
                    {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void renderHUD(MatrixStack matrixStack, ItemStack hudItemStack) {
                PlayerEntity player = Minecraft.getInstance().player;
                ItemStack arrowStack = null;
                ArrowItem arrowItem = null;
                int count = 0;
                int countNext = 0;
                boolean next = false;
                for (int i = 0; i < player.inventory.getSizeInventory(); ++i)
                {
                    ItemStack itemstack = player.inventory.getStackInSlot(i);

                    if (itemstack.getItem() instanceof ArrowItem)
                    {
                        if(itemstack.getItem() instanceof TippedArrowItem){
                            if(count == 0) {
                                arrowStack = itemstack;
                                count = itemstack.getCount();
                                break;
                            }
                            //HUDHelper.renderItemInOffhandHud(itemstack, itemstack.getCount(), 16777215);
                            //return;
                        }
                        if(arrowItem == null)
                            arrowItem = (ArrowItem) itemstack.getItem();
                        if(itemstack.getItem() == arrowItem){
                            count += itemstack.getCount();
                        }else if(itemstack.getItem() != arrowItem && !next){
                            countNext += count;
                            next = true;
                        }
                    }
                }
                HUDHelper.renderItemOppositeOffhandHud(matrixStack, arrowStack == null ? new ItemStack(arrowItem): arrowStack, String.valueOf(count), 16777215);
                if(next){
                    Minecraft minecraft = Minecraft.getInstance();
                    HandSide side = minecraft.player.getPrimaryHand().opposite();
                    String countS = String.valueOf(countNext);
                    MainWindow sr = minecraft.mainWindow;
                    int i = sr.getScaledWidth() / 2;
                    int l1 = sr.getScaledHeight() - 16 - 3;
                    if (side == HandSide.RIGHT) {
                        GlStateManager.disableDepthTest();
                        if(count > 1000) {
                            i -= 6;
                        }
                        minecraft.fontRenderer.drawStringWithShadow(matrixStack, countS, i - 122 - minecraft.fontRenderer.getStringWidth(countS), l1 + 6 + 3, 16777215);
                    } else {
                        GlStateManager.disableDepthTest();
                        if (countNext < 10){
                            i += 6;
                        }else if(countNext < 100){
                            i += 12;
                        }else if(countNext < 1000){
                            i += 18;
                        }else{
                            i += 24;
                        }
                        minecraft.fontRenderer.drawStringWithShadow(matrixStack, countS, i + 124 - minecraft.fontRenderer.getStringWidth(countS), l1 + 6 + 3, 16777215);
                    }
                    GlStateManager.enableDepthTest();
                }
            }
        });
    }
}
