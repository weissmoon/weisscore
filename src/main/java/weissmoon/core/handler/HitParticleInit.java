package weissmoon.core.handler;

import net.minecraft.block.BlockState;
import net.minecraft.client.particle.DiggingParticle;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.world.World;

/**
 * Used to create a ring of particles similar to when a player falls
 * Created by Weissmoon on 6/10/16.
 */
public class HitParticleInit extends DiggingParticle {

    public HitParticleInit(ClientWorld worldIn, double xCoordIn, double yCoordIn, double zCoordIn, double xSpeedIn, double ySpeedIn, double zSpeedIn, BlockState state) {
        super(worldIn, xCoordIn, yCoordIn, zCoordIn, xSpeedIn, ySpeedIn, zSpeedIn, state);
    }
}
