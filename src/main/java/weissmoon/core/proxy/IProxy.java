package weissmoon.core.proxy;

import net.minecraft.entity.Entity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.DimensionType;
import weissmoon.core.client.render.IIconRegister;

public abstract interface IProxy{
    void playBoundSound (String paramBoundSoundCore, SoundCategory category, Entity paramEntity, float loudness, float pitch);

    void registerEventHandler ();

    void initRenderer ();

    void initIcons ();

    void nanoSounds (int paramInt);

    void spawnBlockparticles (RayTraceResult movingObjectPosition, DimensionType dimID, Vector3d vec3d, boolean fancyOnly);

    void registerKeyHandler();

    void registerRenderOverride();

    void initAPIRender();
}
