package weissmoon.core.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.client.settings.GraphicsFanciness;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.DimensionType;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.client.event.ClientEvents;
import weissmoon.core.client.render.WeissRenderItem;
import weissmoon.core.client.render.renderOverride.BridgeModel;
import weissmoon.core.client.render.renderOverride.CustomRenderRegistry;
import weissmoon.core.client.event.RenderEvents;
import weissmoon.core.client.render.renderOverride.PublicRenderRegistry;
import weissmoon.core.client.sound.BoundSoundCore;
import weissmoon.core.control.KeyInputHandler;
import weissmoon.core.control.Keybinds;
import weissmoon.core.handler.HUDItemRealRegistry;
import weissmoon.core.handler.HitParticleInit;
import weissmoon.core.lib.Strings;
import weissmoon.core.lib.TexturesCore;

/**
 * Client side only logic
 */
public class ClientProxy extends CommonProxy{

    public static final IBakedModel dustBa = new BridgeModel();

    public static final WeissRenderItem weissRenderItem = new WeissRenderItem();

    public void playBoundSound (String sound, SoundCategory category, Entity entity, float loudness, float pitch){
        ISound sound1 = new BoundSoundCore(new ResourceLocation(sound), category, entity, loudness, pitch);
        Minecraft.getInstance().getSoundHandler().play(sound1);
    }

    public void registerEventHandler (){
        MinecraftForge.EVENT_BUS.register(new ClientEvents());
    }

    public void initRenderer (){
        //IIconRegister.INSTANCE.registerWeissItemIcons();
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(new RenderEvents());
    }

    @Override
    public void initIcons() {
        //IIconRegister.registerWeissItemIcons();
    }

    public void nanoSounds (int soundID){
    }

    public void spawnBlockparticles (RayTraceResult movingObjectPosition, DimensionType dimID, Vector3d vec3d, boolean fancyOnly){
        if (!fancyOnly || (Minecraft.getInstance().gameSettings.graphicFanciness != GraphicsFanciness.FAST  && fancyOnly))
        if (movingObjectPosition != null && movingObjectPosition.getType() == RayTraceResult.Type.BLOCK)
            new HitParticleInit(
                    Minecraft.getInstance().world,
                    movingObjectPosition.getHitVec().x,
                    movingObjectPosition.getHitVec().y,
                    movingObjectPosition.getHitVec().z,
                    vec3d.x,
                    vec3d.y,
                    vec3d.z,
                    Minecraft.getInstance().world.getBlockState(new BlockPos(movingObjectPosition.getHitVec())));
    }

    @Override
    public void registerKeyHandler(){
        MinecraftForge.EVENT_BUS.register(new KeyInputHandler());
        ClientRegistry.registerKeyBinding(Keybinds.mode);
        ClientRegistry.registerKeyBinding(Keybinds.propel);
        //ClientRegistry.registerKeyBinding(Keybinds.openslot);
    }

    public void registerRenderOverride(){
        CustomRenderRegistry.registerToModelManager();
    }

    @Override
    public void initAPIRender(){
        WeissCoreAPI.renderRegistry = new PublicRenderRegistry();
        WeissCoreAPI.hudItemRegistry = new HUDItemRealRegistry();
    }

    public void onModelBakeEvent(ModelBakeEvent modelBakeEvent){
        //modelBakeEvent.getModelRegistry().putObject(new ModelResourceLocation(Strings.RESOURCE_PREFIX.toLowerCase() + "model/itemCrystal.obj"), this.dustBa);
        modelBakeEvent.getModelRegistry().put(new ModelResourceLocation(Strings.RESOURCE_PREFIX + "renderoverride", "inventory"), this.dustBa);
        modelBakeEvent.getModelRegistry();
        CustomRenderRegistry.registerToModelManager();
        //modelBakeEvent.getModelLoader().setupModelRegistry();
//        for (ModelResourceLocation e: modelBakeEvent.getModelRegistry().getKeys()){
//            if (modelBakeEvent.getModelRegistry().getObject(e) == modelBakeEvent.getModelManager().getMissingModel()){
//                modelBakeEvent.getModelRegistry().putObject(e, modelBakeEvent.getModelManager().getModel(new ModelResourceLocation("" , "inventory")));
//            }
//            if (modelBakeEvent.getModelRegistry().getObject(e).getParticleTexture() == Minecraft.getMinecraft().getTextureMapBlocks().getMissingSprite()){
//                modelBakeEvent.getModelRegistry().putObject(e, modelBakeEvent.getModelManager().getModel(new ModelResourceLocation("" , "inventory")));
//            }
//        }
    }

    public static void init(TextureStitchEvent.Pre event) {

        //optifine breaks (removes) forge added method setTextureEntry, dont use it

        event.addSprite(new ResourceLocation(TexturesCore.CRYSTAL_ALPHA.toString()));
    }

}
